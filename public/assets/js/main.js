window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 15 || document.documentElement.scrollTop > 15) {
        $(".navbar").css('border-bottom', '1px solid #252525')
    } else {
        $(".navbar").css('border-bottom', 'none')
    }
}

function FlipOption() {

    if ($('.option_btn').find('button').hasClass('showed')) {
        $('.option_btn').find('button').removeClass('showed');
        $('.option_block').hide();
    } else {
        $('.option_btn').find('button').addClass('showed');
        $('.option_block').show();
    }

    $("#image").toggleClass('flip');
}

$(document).on("click", ".verifications_menu_btn__container", function (e) {
    e.preventDefault();

    $('.btn-saturn').removeClass('btn-saturn');
    $(e.target).addClass('btn-saturn');
});

$(document).on("click", ".buy_menu_btn__container .type_category .cryptocurrencies_block a", function (e) {
    e.preventDefault();

    var data = $(e.target).data('data');
    $('#selected_crypto').attr('src', data.icon);

    $('.crypto_price').each(function () {
        $(this).html(($(this).data('usd') / data.price_usd).toFixed(6));
    });

    $('.selected_crypto').removeClass('selected_crypto');
    $(e.target).addClass('selected_crypto');

    $('.credit_crypto_img').attr('src', data.icon);
});

$(document).on('click', '.custom_popover', function (e) {
    e.preventDefault()
});

$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
    $('#call_block').hide()

    $('.invalid-feedback').hide()
});

function copyText(el) {
    /* Get the text field */
    var copyText = document.getElementById(el);

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    alert("Copied the text: " + copyText.value);
}

$(document).on('click', '#btn_start_record', function (e) {
    e.preventDefault();
    $(this).hide();
    $('#call_block').show()
});

$(document).on('click', '#btn_cancel_record', function (e) {
    e.preventDefault();
    $('#call_block').hide()
    $('#btn_start_record').show()
});

$(document).on('click', '#btn_logout', function (e) {
    e.preventDefault();

    const hitURL = baseURL + "/logout";

    $.ajax({
        type: 'POST',
        url: hitURL,
        data: {},
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success() {
            window.location.reload();
        }
    });
});

function getVerifications() {

    const hitURL = baseURL + "/getVerificationsBlock";

    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        url: hitURL,
        data: {
            date: getCurrentDate()
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
            $('.content_table table tbody').fadeOut()
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {
            $('.loader_block').fadeOut("slow");
            $('.content_table table tbody').fadeIn("slow").html(data.content);
            $('.zipDrop').html(data.zip);

            setBuyLink()

            $(".search_key__input__inner").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $("#verification_table tbody tr").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $("#search_zip").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $(".zipDrop.dropdown-menu li").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $('.zipDrop li').not(".disabled").on('click', function () {
                $('.selected.active').removeClass('selected active disabled');
                $(this).addClass('selected active');

                $('#dropdownZipButton').find('span').text('Zipcode: ' + $(this).find('.text').text());
                $('#dropdownZipButton').find('span').data('code', $(this).find('.text').text());
            });
        }
    });
}

function addToFavourite(id) {

    const hitURL = baseURL + "/addToFavourite";

    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: hitURL,
        data: {
            targetID: id
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {
            if (data.success === 1) {
                $('#target_' + id).find('.favourite_btn').html('Added');
                // $('#target_' + id).find('.favourite_btn').attr('onclick', "javascript: void(0);");
                $('#target_' + id).find('.favourite_btn').addClass('added');
            } else if (data.success === 2) {
                $('#target_' + id).find('.favourite_btn').html('Add');
                $('#target_' + id).find('.favourite_btn').removeClass('added');
            }
        }
    });
}

$(document).on('click', '#purchase_btn', function (e) {
    e.preventDefault();

    const hitURL = baseURL + "/page_purchase";

    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: hitURL,
        data: {
            currency: $('.selected_crypto').data('id'),
            id: $(this).data('target_id'),
            crypto_amount: $(this).parent().find('.crypto_price').html()
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {
            $('.content_block').html(data.content);
        }
    });
});

function getCurrentDate(addHour = 0) {
    var date = new Date();

    date.setHours(date.getHours() + addHour);
    // return date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()
    // return date.toLocaleString().replace('T',' ').split('.')[0];
    return date.toLocaleString();
}

$(document).on('click', '#btn_pay_by_purchase', function (e) {
    e.preventDefault();

    const hitURL = baseURL + "/page_payment";

    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: hitURL,
        data: {
            currency: $('.selected_crypto').data('id'),
            currency_amount: $('.selected_crypto_amount').data('amount'),
            id: $(this).data('target_id'),
            timestamp: getCurrentDate(10)
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {
            if (data.status == 1) {
                window.open(data.url, '_blank');
            } else {
                let message = JSON.parse(data.data.data.message);
                showResultAlert(data.status, message.amount)
            }
        }
    });
});

$(document).on('click', '#edit_user', function (e) {
    e.preventDefault();

    const hitURL = baseURL + "/" + $(this).data('page');

    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: hitURL,
        data: {
            'login': $('.account__input_inner #login_input').val(),
            'contact': $('#account_input').val(),
            'contact_type': $('.selected_contact_type').data('type'),
            'notification_balance': $('.account__input_inner #notification_balance').val(),
            'notification_state': $('input:checkbox').prop("checked"),
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {

            if (typeof data.errors !== 'undefined') {

                var errors = {
                    'login': {
                        'item': '#login_input',
                        'error': '#login_error',
                        'error_class': 'error_input',
                        'parent': true
                    },
                    'notification_balance': {
                        'item': '#notification_balance',
                        'error': '#notification_balance_error',
                        'error_class': 'error_input',
                        'parent': true
                    }
                };

                checkError('.email_block', errors, data);
            }

            showResultAlert(data.status)
        }
    });
});

$(document).on('click', '.history-container table tbody tr', function (e) {
    if ($(this).data('invoiceurl')) {
        window.open($(this).data('invoiceurl'), '_blank');
    }
});

$(document).on('click', '#update_password', function (e) {
    e.preventDefault();

    const hitURL = baseURL + "/" + $(this).data('page');

    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: hitURL,
        data: {
            'password': $('#password').val(),
            'repeat_password': $('#repeat_password').val(),
            'new_password': $('#new_password').val(),
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {

            if (typeof data.errors !== 'undefined') {

                var errors = {
                    'password': {
                        'item': '#password',
                        'error': '#password_error',
                        'error_class': 'error_input',
                        'parent': true
                    },
                    'password_confirm': {
                        'item': '#repeat_password',
                        'error': '#repeat_password_error',
                        'error_class': 'error_input',
                        'parent': true

                    },
                    'new_password': {
                        'item': '#new_password',
                        'error': '#new_password_error',
                        'error_class': 'error_input',
                        'parent': true
                    },
                };

                checkError('.password_block', errors, data);
            }

            showResultAlert(data.status)
        }
    });
});

function showResultAlert(status, msg = '') {

    $('#success_result').find('div').show()
    $('#error_result').find('div').show()

    if (msg.length == 0) {
        $('#success_result').find('div').hide()
        $('#error_result').find('div').hide()
    }

    if (status == true) {
        $(' #success_result').find('div').text(msg);
        $(' #success_result').fadeIn(500).delay(1000).fadeOut(500)
    } else {
        $(' #error_result').find('div').text(msg);
        $(' #error_result').fadeIn(500).delay(1000).fadeOut(500)
    }
}

function checkError(block, errors, data) {
    Object.keys(errors).forEach(function (key) {

        if (data.errors[key] !== undefined) {
            var item = $(block).find(errors[key].item);

            if (errors[key].parent) {
                item = item.parent();
            }
            item.addClass(errors[key].error_class);
            $(block).find(errors[key].error).text(data.errors[key].toString());
            $(block).find(errors[key].error).show(500);

            setTimeout(function () {
                item.removeClass(errors[key].error_class);
                $(block).find(errors[key].error).hide(500);
                $(block).find(errors[key].error).text = '';
            }, 4000)
        }
    });
}

$(".change_language_btn").click(
    function (event) {
        event.preventDefault();

        const hitURL = baseURL + "/changeLanguage";

        $.ajax({
            type: 'GET',
            url: hitURL,
            data: {
                'lang': $(this).data('lang')
            },
            beforeSend: function () {
                $('#loadingDivBlock').show();
            },
            complete: function () {
                setTimeout(function () {
                    $('#loadingDivBlock').hide();
                }, 1000);
            },
            success() {
                window.location.reload()
            }
        });
    }
);

$('#clearServices').on('click', function () {
    $('#selected_services').text('');
    $('#selected_services').data('count', 0);
    $('#block_selected_services').fadeOut('slow');
});

function setBuyLink() {
    $(".buyVerificationLink").click(
        function (event) {
            event.preventDefault();

            if (!$('#selected_services').text().includes($(this).find('span').text())) {
                var count = parseInt($('#selected_services').data('count')) + 1;
                $('#selected_services').data('count', count);


                $('#selected_services').text($('#selected_services').text() + ($('#selected_services').text() ? ', ' : '') + $(this).find('span').text());

                $('#block_selected_services').fadeIn('slow');
                $('#getPhoneNumber span').html('Request Phone Number (' + count + '<img src="http://localhost/assets/img/coast_veridications.svg" alt="Image">' + ')');
                // showResultAlert(true, successAdded)
            }
            else {
                // showResultAlert(false, errorAdded)
            }
        }
    );
}

$("#getPhoneNumber").click(
    function (event) {
        event.preventDefault();

        const hitURL = baseURL + "/createRentalOrderPreview";

        let id = $('.review_rental_first_block').data('id');

        $.ajax({
            type: 'POST',
            url: hitURL,
            dataType: 'JSON',
            data: {
                'services': $('#selected_services').text(),
                'zip': $('#dropdownZipButton').find('span').data('code')
            },
            beforeSend: function () {
                $('#loadingDivBlock').show();
            },
            complete: function () {
                setTimeout(function () {
                    $('#loadingDivBlock').hide();
                }, 1000);
            },
            success(data) {
                if (data.status == 1) {
                    window.open(data.url, '_self');
                } else {
                    showResultAlert(false, '');
                }
            }
        });
    }
);

$(".cancel_btn").click(
    function (event) {
        event.preventDefault();

        const hitURL = baseURL + "/cancelVerification";

        let id = $('.review_rental_first_block').data('id');

        $.ajax({
            type: 'POST',
            url: hitURL,
            dataType: 'JSON',
            data: {
                'id': id
            },
            beforeSend: function () {
                $('#loadingDivBlock').show();
            },
            complete: function () {
                setTimeout(function () {
                    $('#loadingDivBlock').hide();
                }, 1000);
            },
            success(data) {
                if (data.status == 1) {
                    window.location = baseURL + "/verification";
                }
            }
        });
    }
);

$(".report_number_btn").click(
    function (event) {
        event.preventDefault();

        const hitURL = baseURL + "/reportVerification";

        let id = $('.review_rental_first_block').data('id');

        $.ajax({
            type: 'POST',
            url: hitURL,
            dataType: 'JSON',
            data: {
                'id': id
            },
            beforeSend: function () {
                $('#loadingDivBlock').show();
            },
            complete: function () {
                setTimeout(function () {
                    $('#loadingDivBlock').hide();
                }, 1000);
            },
            success(data) {
                if (data.status == 1) {
                    window.location = baseURL + "/verification";
                }
            }
        });
    }
);

$('#surgePricingSwitch').change(function () {
    if (this.checked) {
        $("#verification_table tbody tr").filter(function () {
            $(this).toggle($(this).data('surge') == false)
        });
    } else {
        $("#verification_table tbody tr").filter(function () {
            $(this).toggle(true)
        });
    }
});

$(document).on("click", ".changeContactType", function (e) {
    e.preventDefault();
    $('.selected_contact_type').removeClass('selected_contact_type');
    $(this).addClass('selected_contact_type');
    $('.selected_contact_type_view').attr('src', $(this).find('img').attr('src'));
});


$('#editUserModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);

    var login = button.data('login');
    var secret_key = button.data('secret_key');
    var balance = button.data('balance');
    var contact = button.data('contact');
    var contact_type = button.data('contact_type');
    var id = button.data('id');

    var modal = $(this);
    modal.find('.modal-body #user_edit_login').val(login);
    modal.find('.modal-body #user_edit_secret_key').val(secret_key);
    modal.find('.modal-body #user_edit_balance').val(balance);
    modal.find('.modal-body #user_edit_contact').val(contact);
    modal.find('.modal-body .selected_contact_type_view').attr('src', baseURL + '/assets/img/' + contact_type + '.svg');
    modal.find('.modal-body #user_edit_id').val(id);
});

$('#editApiUserData').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);

    var token = button.data('token');
    var id = button.data('id');
    var email = button.data('email');
    var method = button.data('method');

    var modal = $(this);
    modal.find('.modal-body #edit_token').val(token);
    modal.find('.modal-body #edit_id').val(id);
    modal.find('.modal-body #edit_method').val(method);
    modal.find('.modal-body #edit_email').val(email);
});

function editApiUser() {
    var token = $('#editApiUserData #edit_token').val();
    var id = $('#editApiUserData #edit_id').val();
    var email = $('#editApiUserData #edit_email').val();
    var method = $('#editApiUserData #edit_method').val();

    const hitURL = baseURL + "/" + method;

    $.ajax({
        type: 'POST',
        url: hitURL,
        dataType: 'JSON',
        data: {
            'id': id,
            'token': token,
            'email': email,
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {
            if (data.status === 1) {
                $('#editApiUserData').modal('hide');
                location.reload();
            }
        }
    });
}

$(document).on('click', '#edit_tax', function (e) {
    e.preventDefault();

    const hitURL = baseURL + "/" + $(this).data('page');

    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: hitURL,
        data: {
            'tax': $('#tax_input').val(),
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success() {
            location.reload()
        }
    });
});

$(document).on('click', '.removePost', function (e) {
    e.preventDefault();

    const hitURL = baseURL + "/removePost";

    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: hitURL,
        data: {
            'id': $(this).data('id'),
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success() {
            location.reload()
        }
    });
});

$(document).on('click', '.removeCredit', function (e) {
    e.preventDefault();

    const hitURL = baseURL + "/removeCredit";

    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: hitURL,
        data: {
            'id': $(this).data('id'),
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success() {
            location.reload()
        }
    });
});

$('#editPostModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);

    var title = button.data('title');
    var content = button.data('content');
    var id = button.data('id');
    var mtitle = button.data('mtitle');
    var method = button.data('method');

    var modal = $(this);
    modal.find('.modal-body #edit_post_title').val(title);
    modal.find('.modal-body #edit_post_content').val(content);
    modal.find('.modal-body #edit_post_id').val(id);
    modal.find('.modal-body #edit_post_method').val(method);
    modal.find('.modal-title').text(mtitle);
});

$('#editCreditModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);

    var cost = button.data('cost');
    var usd = button.data('usd');
    var title = button.data('title');
    var bonus = button.data('bonus');
    var method = button.data('method');
    var id = button.data('id');

    var modal = $(this);
    modal.find('.modal-body #edit_credit_cost').val(cost);
    modal.find('.modal-body #edit_credit_usd').val(usd);
    modal.find('.modal-body #edit_credit_bonus').val(bonus);
    modal.find('.modal-body #edit_credit_id').val(id);
    modal.find('.modal-body #edit_credit_method').val(method);
    modal.find('.modal-title').text(title);
});

function editPost() {

    var title = $('#edit_post_title').val();
    var content = $('#edit_post_content').val();
    var method = $('#edit_post_method').val();
    var id = $('#edit_post_id').val();

    const hitURL = baseURL + "/" + method;

    $.ajax({
        type: 'POST',
        url: hitURL,
        dataType: 'JSON',
        data: {
            'id': id,
            'content': content,
            'title': title,
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {
            if (data.status == 1) {
                location.reload();
            }
        }
    });
}

function editCredit() {

    var bonus = $('#edit_credit_bonus').val();
    var cost = $('#edit_credit_cost').val();
    var method = $('#edit_credit_method').val();
    var id = $('#edit_credit_id').val();
    var usd = $('#edit_credit_usd').val();

    const hitURL = baseURL + "/" + method;

    $.ajax({
        type: 'POST',
        url: hitURL,
        dataType: 'JSON',
        data: {
            'id': id,
            'cost': cost,
            'usd': usd,
            'bonus': bonus,
        },
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {
            if (data.status == 1) {
                location.reload();
            }
        }
    });
}

function getSettingsData() {

    const hitURL = baseURL + "/admin/getSettingsData";

    $.ajax({
        type: 'GET',
        url: hitURL,
        dataType: 'JSON',
        data: {},
        beforeSend: function () {
            $('#loadingDivBlock').show();
        },
        complete: function () {
            setTimeout(function () {
                $('#loadingDivBlock').hide();
            }, 1000);
        },
        success(data) {
            if (data.status == 1) {
                $('.content_table table tbody').html(data.content)
            }
        }
    });
}

