var globalToken;
var type = '1';

$(document).on("click", ".login_link", function (e) {
    e.preventDefault();
    $(e.target).parent().parent().parent().parent().hide();
    $(e.target).parent().parent().parent().parent().siblings().show();
});

function checkError(block, errors, data) {
    Object.keys(errors).forEach(function (key) {

        if (data.errors[key] !== undefined) {
            var item = $(block).find(errors[key].item);

            if (errors[key].parent) {
                item = item.parent();
            }
            item.addClass(errors[key].error_class);
            $(block).find(errors[key].error).text(data.errors[key].toString());
            $(block).find(errors[key].error).addClass('d-block');

            setTimeout(function () {
                item.removeClass(errors[key].error_class);
                $(block).find(errors[key].error).removeClass('d-block');
                $(block).find(errors[key].error).text = '';
            }, 4000)
        }
    });
}

$(document).on("click", ".changeContactType", function (e) {
    e.preventDefault();
    $('.selected_contact_type').removeClass('selected_contact_type');
    $(this).addClass('selected_contact_type');
    $('.selected_contact_type_view').attr('src', $(this).find('img').attr('src'));
});

function onRegister() {

    const hitURL = baseURL + "/register";

    $.ajax({
        type: 'POST',
        url: hitURL,
        data: {
            'login': $('.registration_inner .login__input__inner').val(),
            'password': $('#register_main_password').val(),
            'password_confirm': $('#register_repeat_password').val(),
            'secret_key': $('.registration_inner .secret_key__input__inner').val(),
            'contact': $('.registration_inner .contact__input__inner').val(),
            'contact_type': $('.selected_contact_type').data('type'),
            'h-captcha-response': hcaptcha.getResponse($('#register_captcha iframe').data('hcaptcha-widget-id'))
        },
        dataType: 'json',
        beforeSend: function () {
            $('.registration_inner .spinner-grow').show();
        },
        complete: function () {
            $('.registration_inner .spinner-grow').hide();
        },
        success(data) {

            if (data.status == 1) {
                window.location.reload();
            } else {

                hcaptcha.reset($('#register_captcha iframe').data('hcaptcha-widget-id'));

                if (typeof data.errors !== 'undefined') {

                    var errors = {
                        'login': {
                            'item': '.login__input__inner',
                            'error': '#login_error',
                            'error_class': 'error_input',
                            'parent': true
                        },
                        'password': {
                            'item': '#register_main_password',
                            'error': '#password_error',
                            'error_class': 'error_input',
                            'parent': true

                        },
                        'password_confirm': {
                            'item': '#register_repeat_password',
                            'error': '#repeat_password_error',
                            'error_class': 'error_input',
                            'parent': true

                        },
                        'secret_key': {
                            'item': '.secret_key__input__inner',
                            'error': '#secret_key_error',
                            'error_class': 'error_input',
                            'parent': true

                        },
                        'contact': {
                            'item': '.contact__input__inner',
                            'error': '#contact_error',
                            'error_class': 'error_input',
                            'parent': true

                        },
                        'h-captcha-response': {
                            'item': '.g-recaptcha iframe',
                            'error': '#captcha_error',
                            'error_class': 'error_input border-r-5',
                            'parent': false
                        },
                    };

                    checkError('.registration_inner', errors, data);
                }
            }
        }
    });
};

$(document).ready(function () {

    $('.spinner-grow').hide();

    // Show dropdown
    $('.selected').click(function () {
        $('.custom-sel').addClass('show-sel');
        $('.custom-sel a').removeClass('hidden');
    });

    // Hide dropdown when not focused
    $('.custom-sel').focusout(function () {
        $('.custom-sel').removeClass('show-sel');
        $('.custom-sel a:not(:first)').addClass('hidden');
    }).blur(function () {
        $('.custom-sel').removeClass('show-sel');
        $('.custom-sel a:not(:first)').addClass('hidden');
    });

});

function reload_page() {
    window.location.reload()
}

function onLogin() {

    const hitURL = baseURL + "/login";

    $.ajax({
        type: 'POST',
        url: hitURL,
        data: {
            'login': $('.login__inner .login__input__inner').val(),
            'password': $('.login__inner .password__input__inner').val(),
            'h-captcha-response': hcaptcha.getResponse($('#login_captcha iframe').data('hcaptcha-widget-id'))
        },
        dataType: 'json',
        beforeSend: function () {
            $('.login__inner .spinner-grow').show();
        },
        complete: function () {
            $('.login__inner .spinner-grow').hide();
        },
        success(data) {

            if (data.status == 1) {
                window.location.reload();
            } else {
                if (typeof data.errors !== 'undefined') {

                    hcaptcha.reset($('#login_captcha iframe').data('hcaptcha-widget-id'));

                    var errors = {
                        'login': {
                            'item': '.login__input__inner',
                            'error': '#login_error',
                            'error_class': 'error_input',
                            'parent': true
                        },
                        'password': {
                            'item': '.password__input__inner',
                            'error': '#password_error',
                            'error_class': 'error_input',
                            'parent': true

                        },
                        'h-captcha-response': {
                            'item': '.g-recaptcha iframe',
                            'error': '#captcha_error',
                            'error_class': 'error_input border-r-5',
                            'parent': false
                        },
                    };

                    checkError('.login__inner', errors, data);
                }
            }
        }
    });
}
