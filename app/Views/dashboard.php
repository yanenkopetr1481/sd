<div class="col-10 content_block">
    <?php if ($session->get('isAdmin')) { ?>
        <div class="col-2 d-flex margin-5-0 padding-0">
            <a href="#" id="addBtn" class="purchase_btn_hover btn credit_cart_submit shadow-none"
               data-toggle="modal"
               data-target="#editPostModal" data-id="0"
               data-title=""
               data-method="createPostByAdmin"
               data-mtitle="<?= lang('Dashboard.create') ?>"
               data-content=""><i
                        class="fa fa-plus-circle"
                ></i> <?= lang('Dashboard.create') ?></a>
        </div>
    <?php } ?>
    <div class="card-columns">
        <?php foreach ($data as $card_data) { ?>
            <div class="card">
                <div class="card-body">
                    <p class="card_date"><?= $card_data['created_at'] ?></p>
                    <p class="card_title"><?= $card_data['title'] ?></p>
                    <p class="card_content"><?= $card_data['content'] ?></p>
                    <?php if ($session->get('isAdmin')) { ?>
                        <div class="d-flex margin-5-0 padding-0">
                            <a href="#" class="purchase_btn_hover btn credit_cart_submit shadow-none"
                               data-toggle="modal"
                               data-method="editPostByAdmin"
                               data-target="#editPostModal" data-id="<?= $card_data['id']; ?>"
                               data-title="<?= $card_data['title']; ?>"
                               data-mtitle="<?= lang('Dashboard.edit') ?>"
                               data-content="<?= $card_data['content']; ?>"><i
                                        class="fa fa-edit"></i> <?= lang('Dashboard.edit') ?></a>
                            <a href="#" class="removePost btn shadow-none btn-danger margin-l-5" style="width: 50px;height: 50px"
                               data-id="<?= $card_data['id']; ?>"
                                ><i
                                    class="fa fa-remove"></i></a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div class="modal fade" id="editPostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalLabel"><?= lang('Dashboard.edit') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <label><?= lang('Dashboard.title') ?></label>

                    <div class="account__input_inner">
                        <input class="account__input" type="text" id="edit_post_title"
                               required style="width: 90%"/>
                    </div>

                    <label><?= lang('Dashboard.content') ?></label>
                    <div class="account__input_inner">
                        <input class="account__input" type="text" required id="edit_post_content" style="width: 90%"/>
                    </div>
                    <input type="hidden" id="edit_post_id">
                    <input type="hidden" id="edit_post_method">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-sm btn-secondary"
                        data-dismiss="modal"><?= lang('Admin.close') ?></button>
                <button onclick="editPost()" type="button"
                        class="btn btn-sm selected text-white"><?= lang('Admin.save') ?></button>
            </div>
        </div>
    </div>
</div>

