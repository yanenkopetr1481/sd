<div class="col-10 content_block">
    <div class="review_rental_first_block" data-id="<?= $data['phoneNumber'] ?>">

        <span class="span_title margin-b-24"><?= lang('SMS.review') ?></span>

        <div class="sms_container">

            <div class="verifications__container">
                <div class="container">
                    <div class="row ">
                        <div class="col-sm modal_titles_div">
                            <span class="text-left"><?= lang('SMS.useNumber') ?> <?= implode(' ', $data['currentServices']) ?></span>
                        </div>
                        <div class="col-sm account__input_inner d-flex ">
                            <input id="phone_text" class="sms__input__inner sms_copied"
                                   value="+1 <?= $data['phoneNumber'] ?>">
                            <button class="btn shadow-none" onclick="copyText('phone_text')"><img alt="copied"
                                                                                                  src="<?= base_url('assets/img/copy_btn.svg') ?>">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="verifications__container">
                <div class="container">
                    <div class="row">
                        <div class="col-sm modal_titles_div">
                            <span class="text-left"><?= lang('SMS.remainingTime') ?></span>
                        </div>
                        <div class="col-sm account__input_inner">
                            <input id="date_time_span" disabled class="sms__input__inner" type="text"
                                   value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="verifications__container">
                <div class="container">
                    <div class="row ">
                        <div class="col-sm modal_titles_div">
                            <span class="text-left"><?= lang('SMS.status') ?></span>
                        </div>
                        <div class="col-sm account__input_inner">
                            <input class="sms__input__inner" disabled type="text" value="<?= $data['status'] ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="verifications__container">
                <div class="container">
                    <div class="row align-middle">
                        <div class="col-sm modal_titles_div">
                            <span class="text-left sms_text_title_span"><?= lang('SMS.sms') ?></span>
                        </div>
                        <div class="col-sm account__input_inner d-flex sms_copied_btn" style="height: 200px">
                            <textarea disabled
                                      placeholder="<?= lang('SMS.requestDescription') ?>"
                                      id="sms_text"
                                      class="sms__input__inner sms_textarea"><?php
                                foreach ($data['sms'] as $sms) {
                                    echo $sms['text'];
                                }

                                ?></textarea>
                            <button class="btn shadow-none" onclick="copyText('sms_text')"><img
                                        src="<?= base_url('assets/img/copy_btn.svg') ?>" alt="copied"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);

        function updateClock() {
            var t = getTimeRemaining(endtime);

            clock.value = (('0' + t.minutes).slice(-2) + ':' + ('0' + t.seconds).slice(-2));

            if (t.total <= 0) {
                clock.value = "<?= lang('General.expired') ?>";
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }

    initializeClock('date_time_span', '<?= $data['expirationTime'] ?>');
</script>

