<div class="verifications__container history-container">
    <span class="span_title">Order history</span>
    <div class="row content_table table-responsive">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col"><?= lang('OrderHistory.order') ?></th>
                <th scope="col"><?= lang('OrderHistory.timestamp') ?></th>
                <th scope="col"><?= lang('OrderHistory.cost') ?></th>
                <th scope="col"><?= lang('OrderHistory.paymentType') ?></th>
                <th scope="col"><?= lang('OrderHistory.status') ?></th>
                <th scope="col"><?= lang('OrderHistory.comment')?></th>
                <th scope="col"><?= lang('OrderHistory.amountRequested') ?></th>
                <th scope="col"><?= lang('OrderHistory.amountPaid') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $history_data) { ?>
                <tr data-invoiceUrl="<?= $history_data['invoice_url'] ?>">
                    <td class="align-middle"><span><?= $history_data['id'] ?></span></td>
                    <td class="align-middle"><span
                                id="date_<?= $history_data['id'] ?>_span">
                    <?= $history_data['timestamp'] ?></span>
                    </td>
                    <td class="coast align-middle"><span><?= $history_data['cost'] ?></span><img
                                src="<?= base_url('assets/img/coast_veridications.svg') ?>"
                                alt="Image"><span> (<?= $history_data['usd'] ?>$)</span></td>
                    <td class="align-middle"><span><?= $history_data['payment_type'] ?></span></td>
                    <td class="align-middle"><span> <?= $history_data['status'] ?></span></td>
                    <td class="align-middle"><span> <?= lang('OrderHistory.'.strtolower($history_data['status'])) ?></span></td>
                    <td class="align-middle"><span><?= $history_data['amount_request'] ?></span></td>
                    <td class="align-middle"><span><?= $history_data['amount_paid'] ?></span></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
