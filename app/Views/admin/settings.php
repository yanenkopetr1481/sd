<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">

<div class="verifications__container history-container">
    <div class="row justify-content-between type_category">
        <div class="align-self-stretch">
            <span><?= lang('Admin.settings') ?></span>
            <button type="button" class="btn btn-sm btn-saturn users-btn" data-toggle="modal"
                    data-target="#editApiUserData" data-id=""
                    data-token=""
                    data-email=""
                    data-method="addApiUserByAdmin"
            ><i class="fa fa-plus text-white"></i>
            </button>
        </div>
    </div>
    <div class="row content_table table-responsive ">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col"><?= lang('Admin.id') ?></th>
                <th scope="col"><?= lang('Admin.username') ?></th>
                <th scope="col"><?= lang('Admin.token') ?></th>
                <th scope="col"><?= lang('Admin.balance') ?></th>
                <th scope="col"><?= lang('Admin.action') ?></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="editApiUserData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalLabel"><?= lang('Admin.settings') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="shipping_form">

                    <label><?= lang('Admin.token') ?></label>

                    <div class="account__input_inner d-flex">
                        <input class="account__input" type="text" name="login" id="edit_token"
                               required/>
                    </div>

                    <label><?= lang('Admin.username') ?></label>

                    <div class="account__input_inner d-flex">
                        <input class="account__input" type="text" name="login" id="edit_email"
                               required/>
                    </div>

                    <input type="hidden" class="account__input" name="id" id="edit_id">
                    <input type="hidden" class="account__input" name="id" id="edit_method">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-sm btn-secondary"
                        data-dismiss="modal"><?= lang('Admin.close') ?></button>
                <button onclick="editApiUser()" type="button"
                        class="btn btn-sm selected text-white"><?= lang('Admin.save') ?></button>
            </div>
        </div>
    </div>
</div>


<script>
    document.addEventListener("DOMContentLoaded", function () {
        getSettingsData()
    });
</script>

