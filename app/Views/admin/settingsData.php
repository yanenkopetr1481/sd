<?php foreach ($data as $key => $user_data) { ?>
    <tr>
        <td class="align-middle"><span><?= $user_data['id']; ?></span></td>
        <td class="align-middle"><span><?= $user_data['username']; ?></span></td>
        <td class="align-middle"><span><?= $user_data['token']; ?></span></td>
        <td class="align-middle"><span><?= $user_data['balance']; ?></span></td>
        <td class="align-middle">
            <button type="button" class="btn btn-sm btn-saturn users-btn" data-toggle="modal"
                    data-target="#editApiUserData" data-id="<?= $user_data['id']; ?>"
                    data-token="<?= $user_data['token']; ?>"
                    data-email="<?= $user_data['username']; ?>"
                    data-method="editApiUserByAdmin"
            ><i class="fa fa-edit text-white"></i>
            </button>
        </td>
    </tr>
<?php } ?>
