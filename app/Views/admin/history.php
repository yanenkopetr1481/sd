<div class="verifications__container history-container">
    <div class="row justify-content-between type_category">
        <div class="align-self-stretch">
            <span><?= lang('Verifications.type_title') ?></span>
            <button type="submit" class="btn verifications_menu_btn__container btn-saturn" data-type="0">
                <?= lang('Verifications.type') ?>
            </button>
            <!--            <button type="submit" class="btn verifications_menu_btn__container" data-type="1">Voice</button>-->
        </div>
        <div class="align-self-stretch verifications__title">
            <span><?= lang('Verifications.type_subtitle') ?></span>
        </div>
    </div>
    <div class="row content_table table-responsive">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col"><?= lang('History.timestamp')?></th>
                <th scope="col"><?= lang('History.service')?></th>
                <th scope="col"><?= lang('History.number')?></th>
                <th scope="col"><?= lang('History.type')?></th>
                <th scope="col"><?= lang('History.code')?></th>
                <th scope="col"><?= lang('History.cost')?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $key => $history_data) {
                if ($history_data['data'] != []) { ?>
                    <tr data-invoiceurl=" <?= base_url() . '/verification/' . $history_data['verification_id'] ?>">
                        <td class="align-middle"><span><?= $history_data['updated_at'] ?? $history_data['created_at'] ?></span></td>
                        <td class="service align-middle"><img height="50" width="50"
                                                              src="<?= $history_data['data']['iconUri'] ?>"
                                                              alt="Image"><span><?= $history_data['data']['target_name'] ?></span>
                        </td>
                        <td class="align-middle"><span><?= $history_data['data']['number'] ?></span></td>
                        <td class="align-middle"><span class="status"><?= $history_data['data']['status'] ?></span></td>
                        <td class="favourite align-middle"><a href="#"><?= $history_data['data']['code'] ?></a></td>
                        <td class="coast align-middle"><span><?= $history_data['data']['cost'] ?></span><img
                                    src="<?= base_url('assets/img/coast_veridications.svg') ?>"
                                    alt="Image">
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
</div>
