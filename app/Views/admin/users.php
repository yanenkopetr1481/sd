<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">

<div class="verifications__container history-container">
    <div class="row justify-content-between type_category">
        <div class="align-self-stretch">
            <span><?= lang('Admin.title') ?></span>
        </div>
    </div>
    <div class="row content_table table-responsive ">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col"><?= lang('Admin.id') ?></th>
                <th scope="col"><?= lang('Admin.login') ?></th>
                <th scope="col"><?= lang('Admin.secret_key') ?></th>
                <th scope="col"><?= lang('Admin.balance') ?></th>
                <th scope="col"><?= lang('Admin.contact') ?></th>
                <th scope="col"><?= lang('Admin.created') ?></th>
                <th scope="col"><?= lang('Admin.action') ?></th>
                <th scope="col"><?= lang('Admin.replenishes') ?></th>
                <th scope="col"><?= lang('Admin.history') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $key => $user_data) { ?>
                <tr>
                    <td class="align-middle"><span><?= $user_data['id']; ?></span></td>
                    <td class="align-middle"><span><?= $user_data['login']; ?></span></td>
                    <td class="align-middle"><span><?= $user_data['secret_key']; ?></span></td>
                    <td class="align-middle"><span><?= $user_data['balance']; ?></span></td>
                    <td class="align-middle"><span><?= $user_data['contact']; ?></span></td>
                    <td class="align-middle"><span><?= $user_data['created_at']; ?></span></td>
                    <td class="align-middle">
                        <button type="button" class="btn btn-sm btn-saturn users-btn" data-toggle="modal"
                                data-target="#editUserModal" data-id="<?= $user_data['id']; ?>"
                                data-login="<?= $user_data['login']; ?>"
                                data-secret_key="<?= $user_data['secret_key']; ?>"
                                data-balance="<?= $user_data['balance']; ?>"
                                data-contact="<?= $user_data['contact']; ?>"
                                data-contact_type="<?= $user_data['contact_type']; ?>"
                        ><i class="fa fa-edit text-white"></i>
                        </button>
                    </td>
                    <td class="align-middle">
                        <button type="button" class="btn btn-sm btn-saturn users-btn" onclick="window.open(baseURL + '/admin/history/' + <?= $user_data['id']; ?>)"
                        ><i class="fa fa-eye text-white"></i>
                        </button>
                    </td>
                    <td class="align-middle">
                        <button type="button" class="btn btn-sm btn-saturn users-btn" onclick="window.open(baseURL + '/admin/order/history/' + <?= $user_data['id']; ?>)"
                        ><i class="fa fa-eye text-white"></i>
                        </button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalLabel"><?= lang('Admin.user') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="shipping_form">

                    <label><?= lang('Admin.login') ?></label>

                    <div class="account__input_inner d-flex">
                        <input class="account__input" type="text" name="login" id="user_edit_login"
                               required/>
                    </div>

                    <label><?= lang('Admin.secret_key') ?></label>
                    <div class="account__input_inner d-flex">
                        <input class="account__input" type="text" name="secret_key" required id="user_edit_secret_key"/>
                    </div>

                    <label><?= lang('Admin.balance') ?></label>
                    <div class="account__input_inner d-flex">
                        <input autocomplete="off" type="text" class="account__input" name="balance"
                               id="user_edit_balance"
                        ><span
                                class="input-group-addon"><i
                                    class="glyphicon glyphicon-th"></i></span>
                    </div>
                    <label><?= lang('Admin.contact') ?></label>
                    <div class="account__input_inner d-flex dropdown_parrent">
                        <input class="account__input" type="text" id="user_edit_contact"
                               placeholder="<?= lang('Profile.contact'); ?>...." required>
                        <div class="dropdown contact_type_dropdown">
                            <button class="btn bg-transparent text-white shadow-none d-flex" type="button"
                                    id="dropdownContactType" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false" style="height: 36px">
                                <img class="selected_contact_type_view margin-r-5"
                                     src="<?= base_url('assets/img/' . $user_data['contact_type'] . '.svg') ?>"
                                     alt="language"
                                     height="18" width="18">
                                <img class="margin-5-0"
                                     src="<?= base_url('assets/img/select_arrow.svg') ?>" alt="select">
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownContactType">
                                <a class="dropdown-item changeContactType selected_contact_type padding-t-5"
                                   href="#" data-type="telegram" data-url="telegram.svg"><img class="float-left"
                                                                                              src="<?= base_url('assets/img/telegram.svg') ?>"
                                                                                              alt="language" height="18"
                                                                                              width="18"></a>
                                <a class="dropdown-item changeContactType"
                                   href="#" data-type="jabber"><img class="float-left"
                                                                    src="<?= base_url('assets/img/jabber.svg') ?>"
                                                                    alt="language" height="18" width="18"></a>
                                <a class="dropdown-item changeContactType"
                                   href="#" data-type="mail"><img class="float-left"
                                                                  src="<?= base_url('assets/img/mail.svg') ?>"
                                                                  alt="language" height="18" width="18"></a>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="account__input" name="id" id="user_edit_id">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-sm btn-secondary"
                        data-dismiss="modal"><?= lang('Admin.close') ?></button>
                <button onclick="editUser()" type="button"
                        class="btn btn-sm selected text-white"><?= lang('Admin.save') ?></button>
            </div>
        </div>
    </div>
</div>

