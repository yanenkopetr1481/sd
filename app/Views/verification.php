<div class="verifications__container">
    <div class="row justify-content-between type_category">
        <div class="align-self-stretch">
            <span><?= lang('Verifications.type_title') ?></span>
            <button type="submit" class="btn verifications_menu_btn__container btn-saturn" data-type="0">
                <?= lang('Verifications.type') ?>
            </button>
        </div>
        <div class="align-self-stretch verifications__title">
            <span><?= lang('Verifications.type_subtitle') ?></span>
        </div>
    </div>

    <div class="row">
        <div class="input_inner">
            <input class="search_key__input__inner" type="text"
                   placeholder="<?= lang('General.search') ?>">
        </div>
    </div>

    <div class="row">
        <div class="dropdown nav_account_inner custom filter_numbers  bootstrap-select ">

            <button class="btn bg-transparent text-white shadow-none" type="button"
                    id="dropdownZipButton" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false" style="width: 100%">
                <span class="float-left margin-0-10" data-code="">Zipcode: any</span>
                <img class="float-right margin-10-0"
                     src="<?= base_url('assets/img/select_arrow.svg') ?>" alt="select">
            </button>

            <div class="dropdown-menu" style="max-height: 254px; overflow: hidden; min-height: 40px;padding-top: 0">
                <div class="bs-searchbox"><input placeholder="Search..." id="search_zip" type="search"
                                                 class="form-control" autocomplete="off" role="combobox"
                                                 aria-label="Search" aria-controls="bs-select-2"
                                                 aria-autocomplete="list" aria-activedescendant="bs-select-2-1"></div>
                <div class="inner show" role="listbox" id="bs-select-2" tabindex="-1"
                     style="max-height: 190px; overflow-y: auto;">
                    <ul class="zipDrop dropdown-menu inner show" role="presentation"
                        style="margin-top: 0; margin-bottom: 0;">
                        <!--                        -->
                        <!--                        <li><a role="option" class="dropdown-item"><span class="text">Any</span></a></li>-->
                        <!--                        <li><a role="option"-->
                        <!--                               class="dropdown-item zipcode-option zip-00000"-->
                        <!--                               id="bs-select-2-1" tabindex="0" aria-setsize="99"-->
                        <!--                               aria-posinset="2"><span class="text">00000</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-87121" id="bs-select-2-2"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="3"><span class="text">Albuquerque, NM (87121)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-92805 disabled"-->
                        <!--                                                id="bs-select-2-3" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Anaheim, CA (92805)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-99504" id="bs-select-2-4"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="4"><span-->
                        <!--                                        class="text">Anchorage, AK (99504)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-37013 disabled"-->
                        <!--                                                id="bs-select-2-5" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Antioch, TN (37013)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-30349" id="bs-select-2-6"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Atlanta, GA (30349)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-80013" id="bs-select-2-7"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="6"><span-->
                        <!--                                        class="text">Aurora, CO (80013)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-80015" id="bs-select-2-8"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Aurora, CO (80015)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-93307" id="bs-select-2-9"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Bakersfield, CA (93307)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-97702" id="bs-select-2-10"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Bend, OR (97702)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-35242" id="bs-select-2-11"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Birmingham, AL (35242)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-71111 disabled"-->
                        <!--                                                id="bs-select-2-12" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Bossier City, LA (71111)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-11717" id="bs-select-2-13"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Brentwood, NY (11717)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-06010" id="bs-select-2-14"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Bristol, CT (06010)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-10456 disabled"-->
                        <!--                                                id="bs-select-2-15" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Bronx, NY (10456)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-78521" id="bs-select-2-16"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Brownsville, TX (78521)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-85225" id="bs-select-2-17"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Chandler, AZ (85225)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-37421" id="bs-select-2-18"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Chattanooga, TN (37421)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-82001" id="bs-select-2-19"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Cheyenne, WY (82001)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-34711" id="bs-select-2-20"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Clermont, FL (34711)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-44130" id="bs-select-2-21"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="18"><span class="text">Cleveland, OH (44130)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-12065" id="bs-select-2-22"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="19"><span class="text">Clifton Park, NY (12065)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-88101" id="bs-select-2-23"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="20"><span-->
                        <!--                                        class="text">Clovis, NM (88101)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-77845" id="bs-select-2-24"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="21"><span class="text">College Station, TX (77845)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-38017" id="bs-select-2-25"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Collierville, TN (38017)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-80918" id="bs-select-2-26"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Colorado Springs, CO (80918)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-65203" id="bs-select-2-27"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="24"><span-->
                        <!--                                        class="text">Columbia, MO (65203)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-47201" id="bs-select-2-28"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Columbus, IN (47201)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-72034" id="bs-select-2-29"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Conway, AR (72034)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-51503" id="bs-select-2-30"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Council Bluffs, IA (51503)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-60014" id="bs-select-2-31"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Crystal Lake, IL (60014)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-54115" id="bs-select-2-32"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="29"><span-->
                        <!--                                        class="text">De Pere, WI (54115)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-70726" id="bs-select-2-33"-->
                        <!--                               tabindex="0" aria-setsize="99" aria-posinset="30"><span class="text">Denham Springs, LA (70726)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class=""><a role="option" class="dropdown-item zipcode-option zip-79936" id="bs-select-2-34"-->
                        <!--                                        tabindex="0" aria-setsize="8" aria-posinset="1"><span-->
                        <!--                                        class="text">El Paso, TX (79936)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-11373" id="bs-select-2-35"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Elmhurst, NY (11373)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-05452 disabled"-->
                        <!--                                                id="bs-select-2-36" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Essex Junction, VT (05452)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-97402" id="bs-select-2-37"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Eugene, OR (97402)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-26554" id="bs-select-2-38"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Fairmont, WV (26554)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-28314" id="bs-select-2-39"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Fayetteville, NC (28314)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-98023 disabled"-->
                        <!--                                                id="bs-select-2-40" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Federal Way, WA (98023)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-45840" id="bs-select-2-41"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Findlay, OH (45840)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-40601 disabled"-->
                        <!--                                                id="bs-select-2-42" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Frankfort, KY (40601)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-22407" id="bs-select-2-43"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Fredericksburg, VA (22407)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-93722 disabled"-->
                        <!--                                                id="bs-select-2-44" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Fresno, CA (93722)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-67846" id="bs-select-2-45"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Garden City, KS (67846)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-29445" id="bs-select-2-46"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Goose Creek, SC (29445)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-68801 disabled"-->
                        <!--                                                id="bs-select-2-47" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Grand Island, NE (68801)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-80634" id="bs-select-2-48"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Greeley, CO (80634)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-15601" id="bs-select-2-49"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Greensburg, PA (15601)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-39503" id="bs-select-2-50"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Gulfport, MS (39503)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-92345" id="bs-select-2-51"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Hesperia, CA (92345)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-28601" id="bs-select-2-52"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Hickory, NC (28601)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-49009" id="bs-select-2-53"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Kalamazoo, MI (49009)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-59901" id="bs-select-2-54"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Kalispell, MT (59901)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-64118 disabled"-->
                        <!--                                                id="bs-select-2-55" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Kansas City, MO (64118)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-70065" id="bs-select-2-56"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Kenner, LA (70065)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-76549" id="bs-select-2-57"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Killeen, TX (76549)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-37918" id="bs-select-2-58"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Knoxville, TN (37918)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-17603" id="bs-select-2-59"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Lancaster, PA (17603)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-89110" id="bs-select-2-60"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Las Vegas, NV (89110)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-89123" id="bs-select-2-61"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Las Vegas, NV (89123)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-84043" id="bs-select-2-62"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Lehi, UT (84043)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-68502 disabled"-->
                        <!--                                                id="bs-select-2-63" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Lincoln, NE (68502)</span></a></li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-90805 disabled"-->
                        <!--                                                id="bs-select-2-64" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Long Beach, CA (90805)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-90044" id="bs-select-2-65"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Los Angeles, CA (90044)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-79424" id="bs-select-2-66"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Lubbock, TX (79424)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-75904" id="bs-select-2-67"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Lufkin, TX (75904)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-39110" id="bs-select-2-68"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Madison, MS (39110)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-02148" id="bs-select-2-69"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Malden, MA (02148)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-75070" id="bs-select-2-70"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Mckinney, TX (75070)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-44256" id="bs-select-2-71"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Medina, OH (44256)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-83646" id="bs-select-2-72"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Meridian, ID (83646)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-33186" id="bs-select-2-73"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Miami, FL (33186)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-23112" id="bs-select-2-74"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Midlothian, VA (23112)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-55407" id="bs-select-2-75"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Minneapolis, MN (55407)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-61265" id="bs-select-2-76"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Moline, IL (61265)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-36117" id="bs-select-2-77"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Montgomery, AL (36117)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-56560" id="bs-select-2-78"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Moorhead, MN (56560)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-49442" id="bs-select-2-79"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Muskegon, MI (49442)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-78130" id="bs-select-2-80"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">New Braunfels, TX (78130)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-19720" id="bs-select-2-81"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">New Castle, DE (19720)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-93033" id="bs-select-2-82"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Oxnard, CA (93033)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-99301" id="bs-select-2-83"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Pasco, WA (99301)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-07055" id="bs-select-2-84"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Passaic, NJ (07055)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-02860" id="bs-select-2-85"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Pawtucket, RI (02860)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-19120" id="bs-select-2-86"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Philadelphia, PA (19120)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-77642" id="bs-select-2-87"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Port Arthur, TX (77642)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-27610" id="bs-select-2-88"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Raleigh, NC (27610)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-98052 disabled"-->
                        <!--                                                id="bs-select-2-89" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Redmond, WA (98052)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-89502" id="bs-select-2-90"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Reno, NV (89502)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-13440" id="bs-select-2-91"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Rome, NY (13440)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-95678 disabled"-->
                        <!--                                                id="bs-select-2-92" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Roseville, CA (95678)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-95823" id="bs-select-2-93"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Sacramento, CA (95823)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-56301" id="bs-select-2-94"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Saint Cloud, MN (56301)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-63129 disabled"-->
                        <!--                                                id="bs-select-2-95" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Saint Louis, MO (63129)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-67401" id="bs-select-2-96"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Salina, KS (67401)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-76904" id="bs-select-2-97"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">San Angelo, TX (76904)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-92704 disabled"-->
                        <!--                                                id="bs-select-2-98" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Santa Ana, CA (92704)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-20906" id="bs-select-2-99"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Silver Spring, MD (20906)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-57106" id="bs-select-2-100"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Sioux Falls, SD (57106)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-72764" id="bs-select-2-101"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Springdale, AR (72764)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-95206 disabled"-->
                        <!--                                                id="bs-select-2-102" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Stockton, CA (95206)</span></a></li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-33647" id="bs-select-2-103"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Tampa, FL (33647)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-48180" id="bs-select-2-104"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Taylor, MI (48180)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-84074" id="bs-select-2-105"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Tooele, UT (84074)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-74135" id="bs-select-2-106"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Tulsa, OK (74135)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-31602" id="bs-select-2-107"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Valdosta, GA (31602)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li class="disabled"><a role="option" class="dropdown-item zipcode-option zip-98682 disabled"-->
                        <!--                                                id="bs-select-2-108" aria-disabled="true" tabindex="-1"><span-->
                        <!--                                        class="text">Vancouver, WA (98682)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-20011" id="bs-select-2-109"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Washington, DC (20011)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-95076" id="bs-select-2-110"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Watsonville, CA (95076)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-14580" id="bs-select-2-111"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Webster, NY (14580)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-33411" id="bs-select-2-112"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">West Palm Beach, FL (33411)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-48185" id="bs-select-2-113"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Westland, MI (48185)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-26003" id="bs-select-2-114"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Wheeling, WV (26003)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-17701" id="bs-select-2-115"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Williamsport, PA (17701)</span></a>-->
                        <!--                        </li>-->
                        <!--                        <li><a role="option" class="dropdown-item zipcode-option zip-85364" id="bs-select-2-116"-->
                        <!--                               tabindex="0"><span class=" bs-ok-default check-mark"></span><span class="text">Yuma, AZ (85364)</span></a>-->
                        <!--                        </li>-->
                    </ul>
                </div>
            </div>

        </div>
    </div>

    <div id="block_selected_services" class="row" style="display: none" >

        <div class="col-9" style="padding: 0 10px 0 0;">
            <div class="input_inner d-flex justify-content-between">
                <div class=" selected_services_block">
                    <span>Selected services:</span>
                    <span id="selected_services" data-count="0"></span>
                </div>
                <button id="clearServices" class="btn bg-transparent shadow-none"><i class="fa fa-close text-white"></i>
                </button>
            </div>
        </div>
        <div class="col-3" style="padding: 0">
            <div class="input_inner  btn-saturn">
                <button id="getPhoneNumber" class="btn bg-transparent shadow-none ">
                    <span>Request Phone Number</span>
                </button>
            </div>
        </div>
    </div>

    <div class="row content_table table-responsive">
        <table id="verification_table" class="table table-dark">
            <thead>
            <tr>
                <th scope="col" class="text-left"><?= lang('Verifications.table_service') ?></th>
                <th scope="col" style="width:150px" class="text-center"><?= lang('Verifications.table_status') ?>
                    <a href="#" class="custom_popover" title="Dismissible popover" data-toggle="popover"
                       data-trigger="focus"
                       data-content="Click anywhere in the document to close this popover"><img
                                src="<?= base_url('assets/img/info.svg') ?>"></a>
                </th>
                <th scope="col" style="width:100px"><?= lang('Verifications.table_cost') ?>
                    <a href="#" class="custom_popover" title="Dismissible popover" data-toggle="popover"
                       data-trigger="focus"
                       data-content="Click anywhere in the document to close this popover"><img
                                src="<?= base_url('assets/img/info.svg') ?>"></a>
                </th>
                <th scope="col" style="width:100px"><?= lang('Verifications.table_favorite') ?>
                    <a href="#" class="custom_popover" title="Dismissible popover" data-toggle="popover"
                       data-trigger="focus"
                       data-content="Click anywhere in the document to close this popover"><img
                                src="<?= base_url('assets/img/info.svg') ?>"></a>
                </th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div class="loader_block">
            <div class="bar">
                <div class="circle"></div>
                <p><?= lang('Verifications.loading') ?></p>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        getVerifications()
    });
</script>

