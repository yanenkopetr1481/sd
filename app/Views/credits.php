<div class="verifications__container buy_block buy_menu_btn__container">
    <div class="row justify-content-between type_category margin-0">
        <div class="align-self-stretch">

            <?php if (isset($cryptocurrencies) && ($cryptocurrencies['status'] == 'success')) { ?>
                <div id="operation_selected_block" class="d-flex">
                    <img id="selected_crypto" height='50' width='50' src="<?= $cryptocurrencies['data'][0]['icon'] ?>"
                         alt="Icon" style="margin-right: 10px">
                    <div class="dropdown cryptocurrencies_block">
                        <button class="btn btn-secondary dropdown-toggle btn-saturn" type="button"
                                id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= lang('Credit.select_crypto') ?>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <?php foreach ($cryptocurrencies['data'] as $key => $cryptocurrency) {
                                $class = $key == 0 ? 'selected_crypto' : '';
                                echo "<a class='dropdown-item " . $class . "' data-id='" . $cryptocurrency['cid'] . "' data-data='" . json_encode($cryptocurrency) . "'> <img height='35' width='35' src='" . $cryptocurrency['icon'] . "' alt='icon'> " . $cryptocurrency['name'] . "</a>";
                            } ?>
                        </div>
                    </div>
                    <?php if ($session->get('isAdmin')) { ?>
                        <div class="margin-0-10 cryptocurrencies_block">
                            <a href="#" id="addBtn" class="purchase_btn_hover btn credit_cart_submit shadow-none"
                               data-toggle="modal"
                               data-target="#editCreditModal" data-id="0"
                               data-cost="0"
                               data-title="<?= lang('Credit.create') ?>"
                               data-bonus="0"
                               data-method="createCreditByAdmin"
                               data-usd="0"><i
                                        class="fa fa-plus-circle"></i> <?= lang('Credit.create') ?></a>
                        </div>
                    <?php } ?>
                </div>
            <?php } else {
                echo "<select><option value='BTC'>name</option></select>";
            } ?>

            <!--            <button type="submit" class="btn verifications_menu_btn__container" data-type="calculator_container">-->
            <!--                Calculator-->
            <!--            </button>-->
        </div>
        <div class="align-self-stretch verifications__title">
            <!--            <span id="calculator_title">How much does my verification actually cost?</span>-->
            <!--            <span id="credit_title" class="hide">Credit Bundles</span>-->
        </div>
    </div>

    <!--    <div class="calculator_container hide">-->
    <!--        <div class="container margin-0-7 padding-0">-->
    <!--            <div class="row margin-0">-->
    <!--                <div class="col-8 padding-0">-->
    <!--                    <div class="input_inner">-->
    <!--                        <input class="search_key__input__inner" type="text"-->
    <!--                               placeholder="--><? //= lang('General.search') ?><!--">-->
    <!--                    </div>-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-4 margin-b-24"-->
    <!--                <div class="nav_div">-->
    <!--                    <div class="dropdown nav_account_inner input_inner">-->
    <!--                        <button class="btn bg-transparent shadow-none " type="button"-->
    <!--                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"-->
    <!--                                aria-expanded="false" data-offset="0">-->
    <!--                            400 credits+200 free for $50.00-->
    <!--                            <img src="--><? //= base_url('assets/img/select_arrow.svg') ?><!--" alt="select">-->
    <!--                        </button>-->
    <!--                        <div class="dropdown-menu dropdown-menu-lg-right"-->
    <!--                             aria-labelledby="dropdownMenuButton">-->
    <!--                            <a class="dropdown-item" href="">400 credits+200 free for $50.00</a>-->
    <!--                            <a class="dropdown-item" href="">300 credits+100 free for $40.00</a>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!---->
    <!--        <div class="container margin-0-7 padding-0">-->
    <!--                        <span class="search_under_title">You will pay $1.12 per 1StopMove verification if you purchase the 200 credits+80 free for $25.00 bundle. Calculations exclude <a-->
    <!--                                    href="#">Surge</a> pricing.</span>-->
    <!--        </div>-->
    <!---->
    <!--        <div class="row content_table">-->
    <!--            <table class="table table-dark">-->
    <!--                <thead>-->
    <!--                <tr>-->
    <!--                    <th scope="col" style="width: 33%">Service</th>-->
    <!--                    <th scope="col">Type</th>-->
    <!--                    <th scope="col">Cost-->
    <!--                        <a href="#" class="custom_popover" title="Dismissible popover" data-toggle="popover"-->
    <!--                           data-trigger="focus"-->
    <!--                           data-content="Click anywhere in the document to close this popover"><img-->
    <!--                                    src="--><? //= base_url('assets/img/info.svg') ?><!--" alt="info"></a>-->
    <!--                    </th>-->
    <!--                </tr>-->
    <!--                </thead>-->
    <!--                <tbody>-->
    <!--                <tr>-->
    <!--                    <td class="service align-middle"><img src="-->
    <? //= base_url('assets/img/table_5miles.svg') ?><!--"-->
    <!--                                                          alt="5miles"><span>5miles</span>-->
    <!--                    </td>-->
    <!--                    <td class="align-middle"><span class="status">Text</span></td>-->
    <!--                    <td class="coast align-middle"><span>120</span><img-->
    <!--                                src="-->
    <? //= base_url('assets/img/coast_veridications.svg') ?><!--" alt="Image">-->
    <!--                    </td>-->
    <!--                </tr>-->
    <!--                </tbody>-->
    <!--            </table>-->
    <!--        </div>-->
    <!--    </div>-->
    <div class="credit_container margin-t-24">
        <div class="row">

            <?php foreach ($data as $credit_data) { ?>
                <div class="col-4 margin-b-24">
                    <div class="verifications__container  credit_cart">
                        <div class="d-flex justify-content-between input_inner">
                            <span class="credit_cart_title"><?= lang('Credit.you_acquire') ?></span>
                            <span class="credit_cart_subtitle"><?= $credit_data['bonus'] > 0 ? '+ ' . $credit_data['bonus'] . lang('Credit.free') : '' ?></span>
                        </div>
                        <div class="credit_cart_sum align-middle">
                            <span><?= $credit_data['cost'] ?></span><img class="margin-bottom-3"
                                                                         src="<?= base_url('assets/img/balance_icon.svg') ?>"
                                                                         alt="balance"> за
                            <span class="crypto_price"
                                  data-usd="<?= $credit_data['usd'] ?>"><?= number_format($credit_data['usd'] / $cryptocurrencies['data'][0]['price_usd'], 6) ?></span>
                            <img class="margin-bottom-3 credit_crypto_img" height='30' width='30'
                                 src="<?= $cryptocurrencies['data'][0]['icon'] ?>" alt="crypto">
                            <?= $credit_data['usd'] ?>($)
                        </div>
                        <div class="d-flex">
                            <a href="#" id="purchase_btn" class="purchase_btn_hover btn credit_cart_submit shadow-none"
                               data-target_id="<?= $credit_data['id'] ?>"><?= lang('Credit.purchase') ?></a>
                            <?php if ($session->get('isAdmin')) { ?>
                                <a href="#" id="addBtn"
                                   class="purchase_btn_hover btn credit_cart_submit shadow-none margin-l-5"
                                   style="width: 50px"
                                   data-toggle="modal"
                                   data-target="#editCreditModal" data-id="<?= $credit_data['id']; ?>"
                                   data-cost="<?= $credit_data['cost']; ?>"
                                   data-title="<?= lang('Credit.edit') ?>"
                                   data-bonus="<?= $credit_data['bonus']; ?>"
                                   data-method="editCreditByAdmin"
                                   data-usd="<?= $credit_data['usd']; ?>"><i
                                            class="fa fa-edit"></i></a>
                                <a href="#" class="removeCredit btn btn-danger shadow-none margin-l-5"
                                   style="width: 50px;height: 50px"
                                   data-id="<?= $credit_data['id']; ?>"
                                ><i
                                            class="fa fa-remove"></i></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            <?php } ?>
        </div>
    </div>
</div>

<div class="modal fade" id="editCreditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalLabel"><?= lang('Credit.edit') ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <label><?= lang('Credit.cost') ?></label>

                    <div class="account__input_inner">
                        <input class="account__input" type="text" id="edit_credit_cost"
                               required style="width: 90%"/>
                    </div>

                    <label><?= lang('Credit.usd') ?></label>
                    <div class="account__input_inner">
                        <input class="account__input" type="text" required id="edit_credit_usd" style="width: 90%"/>
                    </div>

                    <label><?= lang('Credit.bonus') ?></label>
                    <div class="account__input_inner">
                        <input class="account__input" type="text" required id="edit_credit_bonus" style="width: 90%"/>
                    </div>
                    <input type="hidden" id="edit_credit_id">
                    <input type="hidden" id="edit_credit_method">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-sm btn-secondary"
                        data-dismiss="modal"><?= lang('Admin.close') ?></button>
                <button onclick="editCredit()" type="button"
                        class="btn btn-sm selected text-white"><?= lang('Admin.save') ?></button>
            </div>
        </div>
    </div>
</div>



