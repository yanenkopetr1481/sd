<div class="row">
    <div class="email_block col-4">
        <div class="verifications__container padding-b-24">
            <span class="span_title margin-b-24"><?= lang('Profile.profile'); ?></span>
            <span class="span_subtitle"><?= lang('Profile.username'); ?></span>
            <div class="account__input_inner d-flex">
                <input class="account__input" type="text" placeholder="<?= lang('Profile.username'); ?>...."
                       value="<?= $user_data['login']; ?>" id="login_input">
            </div>
            <div class="invalid-feedback" id="login_error"></div>

            <br>

            <span class="span_subtitle"><?= lang('Profile.contact'); ?></span>
            <div class="account__input_inner d-flex dropdown_parrent">
                <input class="account__input" type="text" id="account_input" value="<?= $user_data['contact']; ?>" placeholder="<?= lang('Profile.contact'); ?>...." required>
                <div class="dropdown contact_type_dropdown">
                    <button class="btn bg-transparent text-white shadow-none d-flex" type="button"
                            id="dropdownContactType" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" style="height: 36px">
                        <img class="selected_contact_type_view margin-r-5" src="<?= base_url('assets/img/'.$user_data['contact_type'].'.svg') ?>" alt="language"
                             height="18" width="18">
                        <img class="margin-5-0"
                             src="<?= base_url('assets/img/select_arrow.svg') ?>" alt="select">
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownContactType">
                        <a class="dropdown-item changeContactType selected_contact_type padding-t-5"
                           href="#" data-type="telegram" data-url="telegram.svg"><img class="float-left"
                                                                                      src="<?= base_url('assets/img/telegram.svg') ?>"
                                                                                      alt="language" height="18"
                                                                                      width="18"></a>
                        <a class="dropdown-item changeContactType"
                           href="#" data-type="jabber"><img class="float-left"
                                                            src="<?= base_url('assets/img/jabber.svg') ?>"
                                                            alt="language" height="18" width="18"></a>
                        <a class="dropdown-item changeContactType"
                           href="#" data-type="mail"><img class="float-left"
                                                          src="<?= base_url('assets/img/mail.svg') ?>"
                                                          alt="language" height="18" width="18"></a>
                    </div>
                </div>
            </div>
            <div class="invalid-feedback" id="contact_error"></div>
        </div>
        <div class="verifications__container margin-t-24">
            <div class="row justify-content-between margin-0 margin-b-24">
                <span class="span_title"><?= lang('Profile.notification'); ?></span>
                <div class="align-self-stretch verifications__title">
                    <label class="switch">
                        <input type="checkbox" <?= $user_data['notification'] ? 'checked' : '' ?>>
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>
            <span class="span_subtitle"><?= lang('Profile.balanceBelow'); ?></span>
            <div class="account__input_inner d-flex">
                <input class="account__input" type="number" placeholder="0" min="0"
                       value="<?= $user_data['notification_balance'] ?? 0 ?>" id="notification_balance">
            </div>
            <div class="invalid-feedback" id="notification_balance_error"></div>

            <a href="#" class="btn btn_inner shadow-none margin-t-24" id="edit_user" data-page="editUser"><?= lang('Profile.save'); ?></a>
        </div>
    </div>
    <div class="password_block col-8">
        <div class="verifications__container">
            <span class="span_title"><?= lang('Profile.changePassword'); ?></span>

            <p class="span_subtitle"><?= lang('Profile.currentPassword'); ?></p>
            <div class="account__input_inner d-flex">
                <input class="account__input password" type="text" id="password">
            </div>
            <div class="invalid-feedback" id="password_error"></div>

            <p class="span_subtitle"><?= lang('Profile.newPassword'); ?></p>
            <div class="account__input_inne d-flexr">
                <input class="account__input password" type="text" id="repeat_password">
            </div>
            <div class="invalid-feedback" id="repeat_password_error"></div>

            <p class="span_subtitle"><?= lang('Profile.confirmedNewPassword'); ?></p>
            <div class="account__input_inner d-flex">
                <input class="account__input password" type="text" id="new_password">
            </div>
            <div class="invalid-feedback" id="new_password_error"></div>

            <a href="#" class="btn btn_inner shadow-none margin-t-24" id="update_password" data-page="editPassword"><?= lang('Profile.updatePassword'); ?></a>
        </div>
    </div>
</div>

