<!DOCTYPE html>
<html lang="en">
<head>
    <title>Saturn</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/css/login.css?v=' . time()) ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/spiner.css?v=' . time()) ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>
    <script src="<?= base_url('assets/js/login.js?v=' . time()) ?>"></script>
    <script src="https://hcaptcha.com/1/api.js" async defer></script>
</head>
<body class="text-center">


<div class="form-signin">
    <img src="<?= base_url('assets/img/login.svg') ?>" alt="">
    <h4 class="intro__title">Авторизация</h4>
    <h1 class="intro__title">Чтобы начать пользоваться сайтом <br> войдите или зарегистрируйтесь.</h1>

    <div class="total__inner">
        <div class="login__inner">
            <div class="input_inner">
                <input class="login__input__inner" type="text" placeholder="Введите ваш логин...">
            </div>
            <div class="invalid-feedback" id="login_error"></div>

            <div class="input_inner">
                <input class="password__input__inner" type="text" placeholder="Введите ваш пароль...">
            </div>
            <div class="invalid-feedback" id="password_error"></div>

            <div id="login_captcha" class="h-captcha margin-t-24 " data-theme="dark" data-sitekey="a320d18e-e3e7-4292-9006-1e4cb7d206f2"></div>
            <div class="invalid-feedback" id="captcha_error"></div>

            <button class="btn btn_inner margin-t-24" onclick="onLogin()">
                <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                Войти в аккаунт
            </button>

            <p>Нет аккаунта? <a class="login_link" href="#">Зарегистрироваться.</a></p>
        </div>

    </div>
</div>

<div class="form-signin" style="display: none">
    <img src="<?= base_url('assets/img/login.svg') ?>" alt="">
    <h4 class="intro__title">Регистрация</h4>
    <h1 class="intro__title">Чтобы начать пользоваться сайтом <br> войдите или зарегистрируйтесь.</h1>

    <div class="total__inner">

        <div class="registration_inner">

            <div class="input_inner">
                <input class="login__input__inner" type="text" placeholder="Введите логин..." >
            </div>
            <div class="invalid-feedback" id="login_error"></div>


            <div class="input_inner">
                <input class="password__input__inner" id="register_main_password" type="text"
                       placeholder="Введите пароль..." >
            </div>
            <div class="invalid-feedback" id="password_error"></div>

            <div class="input_inner">
                <input class="password__input__inner" id="register_repeat_password" type="text"
                       placeholder="Повторите пароль..." >
            </div>
            <div class="invalid-feedback" id="repeat_password_error"></div>

            <div class="input_inner">
                <input class="secret_key__input__inner" type="text"
                       placeholder="Введите секретное слово..." >
            </div>
            <div class="invalid-feedback" id="secret_key_error"></div>

            <div class="input_inner d-flex dropdown_parrent">
                <input class="contact__input__inner" type="text" placeholder="Контакты для связи..." >
                <div class="dropdown contact_type_dropdown">
                    <button class="btn bg-transparent text-white shadow-none" type="button"
                            id="dropdownContactType" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        <img class="selected_contact_type_view" src="<?= base_url('assets/img/telegram.svg') ?>"
                             alt="language"
                             height="18" width="18">
                        <img class=" margin-10-0"
                             src="<?= base_url('assets/img/select_arrow.svg') ?>" alt="select">
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownContactType">
                        <a class="dropdown-item changeContactType selected_contact_type padding-t-5"
                           href="#" data-type="telegram" data-url="telegram.svg"><img class="float-left"
                                                                                      src="<?= base_url('assets/img/telegram.svg') ?>"
                                                                                      alt="language" height="18"
                                                                                      width="18"></a>
                        <a class="dropdown-item changeContactType"
                           href="#" data-type="jabber"><img class="float-left"
                                                            src="<?= base_url('assets/img/jabber.svg') ?>"
                                                            alt="language" height="18" width="18"></a>
                        <a class="dropdown-item changeContactType"
                           href="#" data-type="mail"><img class="float-left"
                                                          src="<?= base_url('assets/img/mail.svg') ?>"
                                                          alt="language" height="18" width="18"></a>
                    </div>
                </div>
            </div>
            <div class="invalid-feedback" id="contact_error"></div>

            <div id="register_captcha" class="h-captcha margin-t-24 " data-theme="dark" data-sitekey="a320d18e-e3e7-4292-9006-1e4cb7d206f2"></div>
            <div class="invalid-feedback" id="captcha_error"></div>

            <button class="btn btn_inner margin-t-24" onclick="onRegister()">
                <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                Зарегистрироваться
            </button>

            <p>Уже есть аккаунт? <a class="login_link" href="#">Ввойти.</a></p>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/js/http_ajax.googleapis.com_ajax_libs_jquery_3.5.1_jquery.js?v=' . time()) ?>"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
        crossorigin="anonymous"></script>
</body>
</html>

