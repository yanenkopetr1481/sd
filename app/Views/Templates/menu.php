<div class="col-2">
    <div class="menu_box__container">
        <div class="row">
            <div class="col-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none"
                     class="svg replaced-svg <?= $selected == 'announcement_page' ? 'selected_menu_svg' : '' ?>">
                    <path d="M14.25 3H3.75C2.92157 3 2.25 3.67157 2.25 4.5V15C2.25 15.8284 2.92157 16.5 3.75 16.5H14.25C15.0784 16.5 15.75 15.8284 15.75 15V4.5C15.75 3.67157 15.0784 3 14.25 3Z"
                          stroke="#5A5A5A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                    <path d="M12 1.5V4.5" stroke="#5A5A5A" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round"></path>
                    <path d="M6 1.5V4.5" stroke="#5A5A5A" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round"></path>
                    <path d="M2.25 7.5H15.75" stroke="#5A5A5A" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round"></path>
                </svg>
            </div>
            <div class="col-10  <?= $selected == 'announcement_page' ? 'selected_menu' : '' ?>">
                <p id="announcement_page"><a href="/dashboard"><?= lang('Menu.Announcements') ?></a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <svg xmlns="http://www.w3.org/2000/svg" style="margin-left: 1px;" width="18" height="18"
                     viewBox="0 0 18 18" fill="none"
                     class="svg replaced-svg  <?= $selected == 'verification_page' ? 'selected_menu_svg' : '' ?>">
                    <g clip-path="url(#clip0)">
                        <path d="M12 15.75V14.25C12 13.4544 11.6839 12.6913 11.1213 12.1287C10.5587 11.5661 9.79565 11.25 9 11.25H3.75C2.95435 11.25 2.19129 11.5661 1.62868 12.1287C1.06607 12.6913 0.75 13.4544 0.75 14.25V15.75"
                              stroke="#5A5A5A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M6.375 8.25C8.03185 8.25 9.375 6.90685 9.375 5.25C9.375 3.59315 8.03185 2.25 6.375 2.25C4.71815 2.25 3.375 3.59315 3.375 5.25C3.375 6.90685 4.71815 8.25 6.375 8.25Z"
                              stroke="#5A5A5A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M12.75 8.25L14.25 9.75L17.25 6.75" stroke="#5A5A5A" stroke-width="2"
                              stroke-linecap="round" stroke-linejoin="round"></path>
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="18" height="18" fill="white"></rect>
                        </clipPath>
                    </defs>
                </svg>
            </div>
            <div class="col-10 <?= $selected == 'verification_page' ? 'selected_menu' : '' ?>">
                <p id="verification_page"><a href="/verification"><?= lang('Menu.Verifications') ?></a></p>
            </div>
        </div>

        <div class="row">
            <div class="col-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none"
                     class="svg replaced-svg <?= $selected == 'buy_page' ? 'selected_menu_svg' : '' ?>">
                    <path d="M4.5 1.5L2.25 4.5V15C2.25 15.3978 2.40804 15.7794 2.68934 16.0607C2.97064 16.342 3.35218 16.5 3.75 16.5H14.25C14.6478 16.5 15.0294 16.342 15.3107 16.0607C15.592 15.7794 15.75 15.3978 15.75 15V4.5L13.5 1.5H4.5Z"
                          stroke="#5A5A5A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                    <path d="M2.25 4.5H15.75" stroke="#5A5A5A" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round"></path>
                    <path d="M12 7.5C12 8.29565 11.6839 9.05871 11.1213 9.62132C10.5587 10.1839 9.79565 10.5 9 10.5C8.20435 10.5 7.44129 10.1839 6.87868 9.62132C6.31607 9.05871 6 8.29565 6 7.5"
                          stroke="#5A5A5A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                </svg>
            </div>
            <div class="col-10 <?= $selected == 'buy_page' ? 'selected_menu' : '' ?>">
                <p id="buy_page"><a href="/buy"><?= lang('Menu.Buy') ?></a></p>
            </div>
        </div>

        <?php if ($session->get('isAdmin')) { ?>
            <div class="row">
                <div class="col-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none"
                         class="svg replaced-svg  <?= $selected == 'users_page' ? 'selected_menu_svg' : '' ?>">
                        <path d="M4.5 1.5L2.25 4.5V15C2.25 15.3978 2.40804 15.7794 2.68934 16.0607C2.97064 16.342 3.35218 16.5 3.75 16.5H14.25C14.6478 16.5 15.0294 16.342 15.3107 16.0607C15.592 15.7794 15.75 15.3978 15.75 15V4.5L13.5 1.5H4.5Z"
                              stroke="#5A5A5A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M2.25 4.5H15.75" stroke="#5A5A5A" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round"></path>
                        <path d="M12 7.5C12 8.29565 11.6839 9.05871 11.1213 9.62132C10.5587 10.1839 9.79565 10.5 9 10.5C8.20435 10.5 7.44129 10.1839 6.87868 9.62132C6.31607 9.05871 6 8.29565 6 7.5"
                              stroke="#5A5A5A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                    </svg>
                </div>
                <div class="col-10 <?= $selected == 'users_page' ? 'selected_menu' : '' ?>">
                    <p id="users_page"><a href="/admin/users"><?= lang('Menu.Users') ?></a></p>
                </div>
            </div>
        <?php } ?>
        <?php if ($session->get('isAdmin')) { ?>
            <div class="row">
                <div class="col-2">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" width="18px" height="18px"
                         class="svg replaced-svg  <?= $selected == 'settings_page' ? 'selected_menu_svg' : '' ?>">
                        <g id="surface22996844">
                            <path style=" stroke:none;fill-rule:nonzero;fill:rgb(35.294119%,35.294119%,35.294119%);fill-opacity:1;"
                                  d="M 7.25 1.5 L 6.882812 3.394531 C 6.265625 3.625 5.695312 3.949219 5.199219 4.359375 L 3.378906 3.734375 L 1.628906 6.765625 L 3.085938 8.03125 C 3.035156 8.347656 3 8.667969 3 9 C 3 9.332031 3.035156 9.652344 3.085938 9.96875 L 1.628906 11.234375 L 3.378906 14.265625 L 5.199219 13.640625 C 5.695312 14.050781 6.265625 14.375 6.882812 14.605469 L 7.25 16.5 L 10.75 16.5 L 11.117188 14.605469 C 11.734375 14.375 12.304688 14.050781 12.804688 13.640625 L 14.621094 14.265625 L 16.371094 11.234375 L 14.914062 9.96875 C 14.964844 9.652344 15 9.332031 15 9 C 15 8.667969 14.964844 8.347656 14.914062 8.03125 L 16.371094 6.765625 L 14.621094 3.734375 L 12.804688 4.359375 C 12.304688 3.949219 11.734375 3.625 11.117188 3.394531 L 10.75 1.5 Z M 9 6 C 10.65625 6 12 7.34375 12 9 C 12 10.65625 10.65625 12 9 12 C 7.34375 12 6 10.65625 6 9 C 6 7.34375 7.34375 6 9 6 Z M 9 6 "/>
                        </g>
                    </svg>
                </div>
                <div class="col-10 <?= $selected == 'settings_page' ? 'selected_menu' : '' ?>">
                    <p id="users_page"><a href="/admin/settings"><?= lang('Menu.Settings') ?></a></p>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
