<?php

use Config\Services;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('assets/css/custom.css?v=' . time()) ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/table_loader.css?v=' . time()) ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">

    <script type="text/javascript">
        var baseURL = "<?= base_url(); ?>";
        var successAdded = "<?= lang('General.success') ?>";
        var errorAdded = "<?= lang('General.error'); ?>";
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Saturn</title>
</head>
<body>

<nav class="navbar sticky-top navbar-collapse navbar-expand-lg">
    <div class="container">

        <!--    <a class="navbar-brand" href="#">Navbar</a>-->
        <a class="navbar-brand" href="/"> <img src="<?= base_url('assets/img/login.svg') ?>" alt="logo"> </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars fa-lg" style="color: white;padding: 10px;"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto"></ul>
            <form class="form-inline my-2 my-lg-0">
                <div class="nav_div">
                    <div class="dropdown nav_lang_inner language">
                        <button class="btn bg-transparent text-white shadow-none" type="button"
                                id="dropdownLanguageButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <img class="float-left" src="<?= base_url('assets/img/language.svg') ?>" alt="language">
                            <span><?= Services::language()->getLocale() == 'ru' ? 'RUS' : 'ENG' ?></span>
                            <img class="float-right margin-10-0"
                                 src="<?= base_url('assets/img/select_arrow.svg') ?>" alt="select">
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownLanguageButton">
                            <a class="dropdown-item change_language_btn <?= Services::language()->getLocale() == 'ru' ? 'text-white' : '' ?>"
                               href="#" data-lang="ru">RUS</a>
                            <a class="dropdown-item change_language_btn <?= Services::language()->getLocale() == 'en' ? 'text-white' : '' ?>"
                               href="#" data-lang="en">ENG</a>
                        </div>
                    </div>
                </div>
                <div class="nav_div">
                    <div class="nav_balance_inner">
                        <button class="btn bg-transparent text-white shadow-none d-flex" type="button"
                                aria-haspopup="true" aria-expanded="false"
                                onclick="window.location.href = baseURL + '/buy';">
                            <?= $balance ?>
                            <img src="<?= base_url('assets/img/balance_icon.svg') ?>">
                        </button>
                    </div>
                </div>
                <div class="nav_div">
                    <div class="dropdown nav_account_inner custom">
                        <button class="btn bg-transparent text-white shadow-none" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <img class="float-left" src="<?= base_url('assets/img/account.svg') ?>" alt="account">
                            <span class="float-left margin-0-10"><?= $login ?></span>
                            <img class="float-right margin-10-0"
                                 src="<?= base_url('assets/img/select_arrow.svg') ?>" alt="select">
                        </button>
                        <div class="dropdown-menu custom" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="/account"
                            ><?= lang('Menu.MyAccount') ?></a>
                            <a class="dropdown-item" href="/history"><?= lang('Menu.History') ?></a>
                            <a class="dropdown-item" href="/orderHistory"><?= lang('Menu.OrderHistory') ?></a>
                            <a class="dropdown-item text-danger" id="btn_logout"
                               href="#"><?= lang('Menu.Logout') ?></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>

<?php if ($activePhone) { ?>
    <div class="container">
        <div class="alert alert-info">
            <strong><?= lang('General.activePhone')?>
            </strong> <a target="_blank" href="<?= base_url() . '/verification/' . $activePhone;?>"><?= $activePhone; ?></a>
        </div>
    </div>
<?php } ?>

<?php if ($user_data['notification'] == 1) { ?>
    <div class="container">
        <div class="alert alert-dark">
            <strong><?= lang('General.balance') . $user_data['notification_balance']?><img class="img" style="margin-top: -3px;" src="<?= base_url('assets/img/balance_icon.svg') ?>" alt="balance"></strong> <a target="_self" style="color: black" href="<?= base_url() . '/buy'?>">  <?= lang('General.fillBalance')?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
<?php } ?>

<div id="loadingDivBlock">
    <div class="loadingDivBlock_loader"></div>
</div>
<div class="result_alert" id="success_result">
    <img src="<?= base_url('assets/img/accept.svg') ?>" height="200px" alt="accept">
    <div class="alert alert-success div_center" role="alert"></div>
</div>

<div class="result_alert" id="error_result">
    <img src="<?= base_url('assets/img/decline.svg') ?>" height="170px" alt="accept">
    <div class="alert alert-danger div_center" role="alert"></div>
</div>
<div class="container">
    <div class="row">
        <?= view('templates/menu', ['selected' => $selected ?? '']) ?>
