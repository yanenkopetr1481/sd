<div class="verifications__container rentals_block">
    <input  class="selected_crypto" type="hidden" data-id="<?= $currency ?>">
    <input  class="selected_crypto_amount" type="hidden" data-amount="<?= $currency_amount ?>">
    <span class="span_title"><?= lang('Credit.checkout')?></span>
    <div class="row account__input_inner margin-0 margin-t-24 checkout_content">
        <div class="col-3 ">
            <span class="span_title"><?= lang('Credit.buying')?></span>
            <span class="span_subtitle"><?= $data['cost'] ?> credits <?= $data['bonus'] > 0 ? '+ ' . $data['bonus'] . ' FREE' : '' ?></span>
        </div>
        <div class="col-6 checkout_sub_content">
            <span class="span_title"><?= lang('Credit.description')?></span>
            <span class="span_subtitle"><?= $data['description'] ?></span>
        </div>
        <div class="col-3 checkout_sub_content">
            <span class="span_title"><?= lang('Credit.price')?></span>
            <span class="span_subtitle"><?= $data['usd'] ?>$</span>
        </div>
    </div>
    <div>
        <a type="submit" href="#" id="btn_pay_by_purchase" data-target_id="<?= $data['id'] ?>" data-page="page_payment" class="btn btn_inner shadow-none margin-r-24">
            <?= lang('Credit.purchase')?>
        </a>
    </div>
</div>
