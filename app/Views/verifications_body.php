<?php foreach ($data as $verification_data) { ?>
    <tr id="target_<?= $verification_data['name'] ?>">
        <td class="service text-left"><img src="<?= $verification_data['iconUri'] ?>"
                                           alt="Image"
                                           style="width: 41px;height: 41px"><a class="buyVerificationLink" href="#" data-id="<?= $verification_data['name'] ?>"><span><?= $verification_data['name'] ?></span></a>
        </td>
        <td class="align-middle">
            <span class="status"><?= $verification_data['status'] ?></span>
        </td>
        <td class="coast align-middle">
                        <span>
                                <?= $verification_data['cost'] ?>
                        </span>
            <img src="<?= base_url('assets/img/coast_veridications.svg') ?>" alt="Image">
        </td>
        <?php

        if ($verification_data['favourite'] === 0) { ?>
        <td class="favourite align-middle"><a href="#" class="favourite_btn added"
                                              onclick="addToFavourite('<?= $verification_data['name'] ?>')"><?= lang('General.added')?></a>
            <?php } else { ?>
        <td class="favourite align-middle"><a href="#" class="favourite_btn"
                                              onclick="addToFavourite('<?= $verification_data['name'] ?>')"><?= lang('General.add')?></a>
            <?php } ?>
        </td>
    </tr>
<?php } ?>
