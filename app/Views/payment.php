<div class="verifications__container payment_block">
    <span class="span_title">Checkout</span>
    <span class="span_subtitle">Please pay the amount below for your order of 1 unit of <?= $data['cost']?> Credits</span>

    <div class="row margin-t-24 margin-b-24">
        <div class="col-2">
            <img id='barcode' src="https://api.qrserver.com/v1/create-qr-code/?data=HelloWorld&amp;size=100x100" alt="" title="0.00055631 BTC" width="155" height="155" />
        </div>
        <div class="col-10 ">
            <p class="span_title"><?= $data['btc']?> BTC</p>
            <p class="span_subtitle">Send exactly <?= $data['cost']?> (in one payment) to:</p>
            <p class="span_payment_title">3GYur3zM7EmDfviuTLoyykfLrUuKZEMzYA</p>
            <p class="span_payment_subtitle">Payment window: </p>
        </div>
    </div>
</div>

<script>
    // Set the date we're counting down to
    var countDownDate = new Date();
    countDownDate.setHours(countDownDate.getHours() + 1);
    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate.getTime() - now;

        // Time calculations for days, hours, minutes and seconds
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        $('.span_payment_subtitle').html( "Payment window: " + hours + ":" +minutes + ":" + seconds);

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            $('.span_payment_subtitle').html("EXPIRED");
        }
    }, 1000);
</script>
