<div class="container text-center">

    <h1 class="headline"><?= lang('General.error_title')?></h1>

    <p class="lead"><?= lang('General.error_subtitle')?></p>

</div>
