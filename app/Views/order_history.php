<script>
    function paymentTimer(date, item, isFinished) {

        let staticDate = new Date(date);
        // Set the date we're counting down to
        var countDownDate = new Date(date);

        // if (isFinished !== 'Completed') {
            // Update the count down every 1 second
            var x = setInterval(function () {

                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate.getTime() - now;

                // Time calculations for days, hours, minutes and seconds
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Output the result in an element with id="demo"
                $(item).html(date + " (" + hours + ":" + minutes + ":" + seconds + ")");

                // If the count down is over, write some text
                if (distance < 0 || isFinished == 'Completed') {
                    clearInterval(x);
                    $(item).html(staticDate.toLocaleString());
                }
            }, 1000);
        // }
        // else {
        //     $(item).html(staticDate.toLocaleString());
        // }
    }
</script>

<div class="verifications__container history-container">
    <span class="span_title">Order history</span>
    <div class="row content_table table-responsive">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col"><?= lang('OrderHistory.order')?></th>
                <th scope="col"><?= lang('OrderHistory.timestamp')?></th>
                <th scope="col"><?= lang('OrderHistory.cost')?></th>
                <th scope="col"><?= lang('OrderHistory.paymentType')?></th>
                <th scope="col"><?= lang('OrderHistory.status')?></th>
                <th scope="col"><?= lang('OrderHistory.comment')?></th>
                <th scope="col"><?= lang('OrderHistory.amountRequested')?></th>
                <th scope="col"><?= lang('OrderHistory.amountPaid')?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $history_data) { ?>
                <tr data-invoiceUrl="<?= $history_data['invoice_url']?>">
                    <td class="align-middle"><span><?= $history_data['id'] ?></span></td>
                    <td class="align-middle"><span
                                id="date_<?= $history_data['id'] ?>_span">
                    <?= '
                         <script> 
                             paymentTimer("' . $history_data['timestamp'] . '", "#date_' . $history_data['id'] . '_span", "' . $history_data['status'] . '") 
                         </script>
                    ' ?></span>
                    </td>
                    <td class="coast align-middle"><span><?= $history_data['cost'] ?></span><img
                                src="<?= base_url('assets/img/coast_veridications.svg') ?>"
                                alt="Image"><span> (<?= $history_data['usd'] ?>$)</span></td>
                    <td class="align-middle"><span><?= $history_data['payment_type'] ?></span></td>
                    <td class="align-middle"><span> <?= $history_data['status'] ?></span></td>
                    <td class="align-middle"><span> <?= lang('OrderHistory.'.strtolower($history_data['status'])) ?></span></td>
                    <td class="align-middle"><span><?= $history_data['amount_request'] ?></span></td>
                    <td class="align-middle"><span><?= $history_data['amount_paid'] ?></span></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
