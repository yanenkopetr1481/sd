<div class="verifications__container history-container">
    <div class="row justify-content-between type_category">
        <div class="align-self-stretch">
            <span><?= lang('Verifications.type_title') ?></span>
            <button type="submit" class="btn verifications_menu_btn__container btn-saturn" data-type="0">
                <?= lang('Verifications.type') ?>
            </button>
        </div>
        <div class="align-self-stretch verifications__title">
            <span><?= lang('Verifications.type_subtitle') ?></span>
        </div>
    </div>
    <div class="row content_table table-responsive">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col"><?= lang('History.timestamp') ?></th>
                <th scope="col"><?= lang('History.service') ?></th>
                <th scope="col"></th>
                <th scope="col"><?= lang('History.zip') ?></th>
                <th scope="col"><?= lang('History.number') ?></th>
                <!--                <th scope="col">--><? //= lang('History.code')?><!--</th>-->
                <th scope="col"><?= lang('History.cost') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach ($data as $key => $history_data) {
                $services = json_decode($history_data['services'], true);

                if ($history_data['data'] != []) { ?>
                    <tr data-invoiceurl=" <?= base_url() . '/verification/' . $history_data['verification_id'] ?>">
                        <td class="align-middle"><span
                                    id="date_time_span_<?= $key ?>"><?= $history_data['created_at'] ?></span></td>
                        <td class="service align-middle">
                            <img height="50" width="50" src="<?= $history_data['data']['iconUri'] ?>" alt="Image">
                        </td><td class="service align-middle">
                            <span><?= implode($services['availableServices'] ?? [], ' ') ?></span>
                        </td>
                        <td class="align-middle"><span><?= $history_data['verification_id'] ?></span></td>
                        <td class="align-middle">

                            <?= $history_data['status'] === '0' ? '<span class="status text-danger"> Refund of ' . count($services['availableServices'] ?? 0 + $services['availableZips'] ?? 0) . ' credits for unused ' . implode($services['availableServices'] ?? [], ' ') . ' at ' . $history_data['verification_id'] . '.</span>' : '<span class="status text-success">New phone number ' . $history_data['verification_id'] . ' for ' . implode($services['availableZips'] ?? [], ' ') . ' usage </span>' ?>

                        </td>
                        <!--                        <td class="favourite align-middle"><a href="#">-->
                        <?//= $history_data['data']['code'] ?><!--</a></td>-->
                        <td class="coast align-middle">
                            <span><?= count($services['availableServices'] ?? 0 + $services['availableZips'] ?? 0) ?></span><img
                                    src="<?= base_url('assets/img/coast_veridications.svg') ?>"
                                    alt="Image">
                        </td>
                    </tr>
                    <script>
                        //initializeClock('date_time_span', '<?//= $data['expirationTime'] ?>//');

                        //orderTimer('<?//= $history_data['data']['time_remaining'] ?>//', '#date_time_span_<?//= $key ?>//', '<?//= $history_data['data']['status'] ?>//');
                    </script>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
</div>
<script>

    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);

        function updateClock() {
            var t = getTimeRemaining(endtime);

            clock.value = (('0' + t.minutes).slice(-2) + ':' + ('0' + t.seconds).slice(-2));

            if (t.total <= 0) {
                clock.value = "<?= lang('General.expired') ?>";
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }
</script>

