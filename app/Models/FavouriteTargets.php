<?php namespace App\Models;

use CodeIgniter\Model;

class FavouriteTargets extends Model
{
    protected $table = 'favouriteTargets';
    protected $allowedFields = ['user_id', 'target_id'];

    protected function beforeInsert(array $data) {}

    protected function beforeUpdate(array $data) {}
}
