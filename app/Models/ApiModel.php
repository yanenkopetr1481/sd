<?php namespace App\Models;

use CodeIgniter\Model;

class ApiModel extends Model
{
    protected $table = 'api_auth';
    protected $allowedFields = ['email', 'token', 'isAvaliable', 'user_id', 'deadline', 'updated_at'];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    protected function beforeInsert(array $data){}

    protected function beforeUpdate(array $data)
    {
        $data['data']['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }
}
