<?php namespace App\Models;

use CodeIgniter\Model;

class CreditModel extends Model
{
    protected $table = 'credits';
    protected $allowedFields = ['cost', 'btc', 'usd', 'bonus', 'description', 'created_at', 'updated_at'];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    protected function beforeInsert(array $data)
    {
        $data['data']['created_at'] = date('Y-m-d H:i:s');
        return $data;
    }

    protected function beforeUpdate(array $data)
    {
        $data['data']['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }
}
