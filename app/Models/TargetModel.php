<?php namespace App\Models;

use CodeIgniter\Model;

class TargetModel extends Model
{
    protected $table = 'targets';
    protected $allowedFields = ['name', 'iconUri'];

    protected function beforeInsert(array $data) {}

    protected function beforeUpdate(array $data) {}
}
