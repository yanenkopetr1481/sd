<?php namespace App\Models;

use CodeIgniter\Model;

class VerificationHistory extends Model
{
    protected $table = 'verifications_history';
    protected $allowedFields = ['id', 'verification_id', 'created_at', 'updated_at', 'user_id', 'token', 'status', 'services'];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    protected function beforeInsert(array $data)
    {
        $data['data']['created_at'] = date('Y-m-d H:i:s');
        return $data;
    }

    protected function beforeUpdate(array $data)
    {
        $data['data']['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }
}
