<?php

return [
    'Announcements' => 'Анонсы',
    'Verifications' => 'Аренда номера',
    'Rentals' => 'Аренда',
    'Buy' => ' Купить',
    'MyAccount' => 'Аккаунт',
    'History' => 'История',
    'OrderHistory' => 'История заказов',
    'Logout' => 'Выйти',
    'Users' => 'Пользователи',
    'Settings' => 'Настройки',
];
