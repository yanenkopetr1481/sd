<?php

return [
    'title' => 'Пользователи Saturn',
    'id' => 'ID',
    'login' => 'Логин',
    'secret_key' => 'Секретное слово',
    'balance' => 'Баланс',
    'contact' => 'Контакт',
    'created' => 'Создан',
    'action' => 'Действия',
    'user' => 'Пользователь',
    'close' => 'Закрыть',
    'save' => 'Сохранить',
    'tax' => 'Умножитель на цену',
    'replenishes' => 'Пополнения',
    'history' => 'История',
    'token' => 'Токен',
    'username' => 'Имя пользователя',
    'settings' => 'Настройки'
];
