<?php

return [
    'type_title' => 'Тип аренды:',
    'type' => 'СМС',
    'type_subtitle' => 'Одноразовая',
    'table_service' => 'Сервис',
    'table_status' => 'Статус',
    'table_cost' => 'Стоимость',
    'table_favorite' => 'Избранные',
    'loading' => 'Загрузка',
];
