<?php

return [
    'profile' => 'Профиль',
    'username' => 'Имя пользователя',
    'notification' => 'Уведомления',
    'changePassword' => 'Изменить пароль',
    'currentPassword' => 'Текущий пароль',
    'newPassword' => 'Новый пароль',
    'confirmedNewPassword' => 'Подтверждение нового пароля',
    'balanceBelow' => 'Уведомить если баланс меньше:',
    'save' => 'Сохранить',
    'updatePassword' => 'Изменить пароль',
    'contact' => 'Контакт'

];
