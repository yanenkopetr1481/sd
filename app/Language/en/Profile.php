<?php

return [
    'profile' => 'Profile',
    'username' => 'Username',
    'notification' => 'Notification',
    'changePassword' => 'Change password',
    'currentPassword' => 'Current password',
    'newPassword' => 'New password',
    'confirmedNewPassword' => 'Confirmed new password',
    'balanceBelow' => 'Alert if balance below:',
    'save' => 'Save',
    'updatePassword' => 'Update password',
    'contact' => 'Contact'
];
