<?php

return [
    'search' => 'Search...',
    'NotAvailable' => 'Not available',
    'Available' => 'Available',
    'SurgePricingBlocked' => 'Surge pricing blocked',
    'QuotaExceeded' => 'Quota exceeded',
    'Default' => 'Default',
    'Surge' => 'Surge',
    'Free' => 'Free',
    'Premium' => 'Premium',
    'Adjusted' => 'Adjusted',
    'Filtered' => 'Filtered',
    'added' => 'Added',
    'add' => 'Add',
    'balance' => 'Balance less than ',
    'select_crypto' => 'Select cryptocurrencies',
    'serverError' => 'Server error. Please try again later',
    'fillBalance' => 'Fill balance',
    'expired' => 'Time expired',
    'error_title' => 'Whoops!',
    'success' => 'Service added',
    'error' => 'Service is selected',
    'activePhone' => 'You have active phone number!',
    'error_subtitle' => 'We seem to have hit a snag. Please try again later...',
];
