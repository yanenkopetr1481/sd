<?php

return [
    'type_title' => 'Verification type:',
    'type' => 'Text & SMS',
    'type_subtitle' => 'Short-term',
    'table_service' => 'Service',
    'table_status' => 'Status',
    'table_cost' => 'Cost',
    'table_favorite' => 'Favorite',
    'loading' => 'Loading',
];
