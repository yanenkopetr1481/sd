<?php

return [
    'timestamp' => 'Timestamp',
    'order' => 'Order No.',
    'service' => 'Service',
    'paymentType' => 'Payment Type',
    'amountRequested' => 'Amount Requested',
    'amountPaid' => 'Amount Paid',
    'cost' => 'Cost',
    'status' => 'Status',
    'comment' => 'Comment',
    'not_full' => 'Not full amount',
    'new' => 'Initial invoice status',
    'expired' => 'The full amount may not have been paid.',
    'pending' => 'some amount received and waiting for confirmations',
    'completed' => 'completed',
    'mismatch' => 'overpaid',
    'error' => 'some error has occurred',
    'cancelled' => 'no payment received within 10 hours',
];
