<?php

return [
    'create' => 'Create new post',
    'edit' => 'Edit post',
    'title' => 'Title',
    'content' => 'Content',
];
