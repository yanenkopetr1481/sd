<?php

return [
    'title' => 'Saturn users',
    'id' => 'ID',
    'login' => 'Login',
    'secret_key' => 'Secret key',
    'balance' => 'Balance',
    'contact' => 'Contact',
    'created' => 'Created',
    'action' => 'Action',
    'user' => 'User',
    'close' => 'Close',
    'save' => 'Save',
    'tax' => 'Tax',
    'replenishes' => 'Replenishes',
    'history' => 'History',
    'token' => 'Token',
    'username' => 'Username',
    'settings' => 'Settings'
];
