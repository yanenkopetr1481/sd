<?php

return [
    'review' => 'Review Rental Order',
    'useNumber' => 'Use this phone number for',
    'remainingTime' => 'Remaining Time',
    'status' => 'Status',
    'sms' => 'SMS',
    'cancel' => 'Cancel',
    'report' => 'Report Number',
    'requestDescription' => 'Please request only one code. Multiple requests may result in issues with your code.',
];
