<?php

return [
    'Announcements' => 'Announcements',
    'Verifications' => 'Verifications',
    'Rentals' => 'Rentals',
    'Buy' => 'Buy',
    'MyAccount' => 'My account',
    'History' => 'History',
    'Users' => 'Users',
    'OrderHistory' => 'Order history',
    'Logout' => 'Logout',
    'Settings' => 'Settings',
];
