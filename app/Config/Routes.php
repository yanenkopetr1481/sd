<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Dashboard');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Login::index', ['filter' => 'noauth']);
$routes->match(['post'],'login', 'Login::login', ['filter' => 'noauth']);
$routes->match(['post'],'register', 'Login::register', ['filter' => 'noauth']);
$routes->match(['post'],'logout', 'Login::logout', ['filter' => 'auth']);


$routes->get('/dashboard', 'Dashboard::index',['filter' => 'auth']);
$routes->get('/buy', 'Dashboard::getCredits',['filter' => 'auth']);
$routes->match(['get'],'changeLanguage', 'Dashboard::changeLanguage',['filter' => 'auth']);
$routes->match(['get'],'getVerificationsBlock', 'Dashboard::getVerificationsBlock',['filter' => 'auth']);
$routes->match(['post'],'addToFavourite', 'Dashboard::addToFavourite', ['filter' => 'auth']);


$routes->get('/verification', 'Payment::getVerifications',['filter' => 'auth']);
$routes->get('/verification/(:any)', 'Payment::getVerificationStatus/$1',['filter' => 'auth']);
$routes->post('/cancelVerification', 'Payment::cancelVerification',['filter' => 'auth']);
$routes->post('/reportVerification', 'Payment::reportVerification',['filter' => 'auth']);
$routes->match(['post'],'createRentalOrderPreview', 'Payment::createRentalOrderPreview',['filter' => 'auth']);
$routes->get('/orderHistory', 'Payment::getOrderHistory',['filter' => 'auth']);
$routes->get('/history', 'Payment::getHistory',['filter' => 'auth']);
$routes->post('page_purchase', 'Payment::getPurchase',['filter' => 'auth']);
$routes->post('page_payment', 'Payment::getPayment',['filter' => 'auth']);

$routes->match(['post','get','put'],'payment/status', 'Psilio::status',['filter' => 'noauth']);
$routes->match(['post','get','put'], 'payment/success', 'Psilio::success',['filter' => 'noauth']);
$routes->match(['post','get','put'],'payment/failed', 'Psilio::failed',['filter' => 'noauth']);

$routes->match(['get'],'admin/users', 'Admin::getUsers',['filter' => 'auth']);
$routes->match(['get'],'admin/settings', 'Admin::getSettings',['filter' => 'auth']);
$routes->match(['get'],'admin/getSettingsData', 'Admin::getSettingsData',['filter' => 'auth']);
$routes->match(['get'],'admin/history/(:any)', 'Admin::getUserHistory/$1',['filter' => 'auth']);
$routes->match(['get'],'admin/order/history/(:any)', 'Admin::getUserOrderHistory/$1',['filter' => 'auth']);
$routes->match(['post'],'editUserByAdmin', 'Admin::editUserByAdmin',['filter' => 'auth']);
$routes->match(['post'],'addApiUserByAdmin', 'Admin::addApiUserByAdmin',['filter' => 'auth']);
$routes->match(['post'],'editApiUserByAdmin', 'Admin::editApiUserByAdmin',['filter' => 'auth']);
$routes->match(['post'],'editPostByAdmin', 'Admin::editPostByAdmin',['filter' => 'auth']);
$routes->match(['post'],'removePost', 'Admin::removePost',['filter' => 'auth']);
$routes->match(['post'],'removeCredit', 'Admin::removeCredit',['filter' => 'auth']);
$routes->match(['post'],'createPostByAdmin', 'Admin::createPostByAdmin',['filter' => 'auth']);
$routes->match(['post'],'editCreditByAdmin', 'Admin::editCreditByAdmin',['filter' => 'auth']);
$routes->match(['post'],'createCreditByAdmin', 'Admin::createCreditByAdmin',['filter' => 'auth']);
$routes->match(['post'],'editTax', 'Admin::editTax',['filter' => 'auth']);

$routes->get('account', 'User::getAccount', ['filter' => 'auth']);
$routes->match(['post'],'editUser', 'User::editUser', ['filter' => 'auth']);
$routes->match(['post'],'editPassword', 'User::editPassword', ['filter' => 'auth']);
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
