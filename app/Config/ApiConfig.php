<?php namespace App\Config;

use CodeIgniter\Config\BaseConfig;

class ApiConfig extends BaseConfig
{
    public $api_url = 'https://www.sms-reception.com/';
}
