<?php

namespace App\Validation;

use App\Models\UserModel;
use Config\Services;

class UserRules
{

    public function validateUser(string $str, string $fields, array $data)
    {
        $model = new UserModel();
        $user = $model->where('login', $data['login'])
            ->first();

        if (!$user) return false;

        return password_verify($data['password'], $user['password']);
    }

    public function validateCaptcha(string $str, string $fields, array $data)
    {
        $options = [
            'baseURI' => 'https://hcaptcha.com/siteverify',
            'timeout' => 3
        ];

        $client = Services::curlrequest($options);

        $response = $client->request('POST', '', [
            'form_params' => [
                'secret' => "0xe57C9186577f27bA3fF948c5540743E3985D6773",
                'response' => $data['h-captcha-response']
            ]
        ]);

        return json_decode($response->getBody(), true)['success'];
    }

    public function validatePassword(string $str, string $fields, array $data)
    {
        $model = new UserModel();
        $user = $model->where('id', session()->get('id'))
            ->first();

        if (!$user) return false;

        return password_verify($data['password'], $user['password']);
    }
}
