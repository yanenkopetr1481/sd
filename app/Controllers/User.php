<?php

namespace App\Controllers;

use App\Models\UserModel;
use Config\Services;
use ReflectionException;

class User extends BaseController
{
    public function index()
    {
        return $this->loadView('dashboard');
    }

    public function getAccount()
    {
        $model = new UserModel();

        $user_data = $model->getWhere(['id' => $this->session->get('id')]);

        if (empty($user_data)) {
            $this->response(['status' => 0, 'msg' => 'user not found']);
            return $this->loadView('errors/html/error');
        }

        return $this->loadView('account', ['user_data' => $user_data->getFirstRow('array')], '<div class="col-10 content_block">', '</div>');
    }

    public function editUser()
    {
        if ($this->request->isAJAX()) {

            $validation = Services::validation();

            $rules = [
                'login' => 'required|min_length[3]|max_length[20]',
                'contact' => 'required|min_length[3]|max_length[60]',
                'contact_type' => 'required|min_length[3]|max_length[20]',
                'notification_balance' => 'required|numeric|greater_than[-1]'
            ];

            $errors = [
                'login' => [
                    'required' => 'Login is missed',
                    'min_length' => 'Login must be more 3 characters',
                    'max_length' => 'Login must be less 50 characters',
                ],
                'contact' => [
                    'required' => 'Contact is missed',
                    'min_length' => 'Contact must be more 3 characters',
                    'max_length' => 'Contact must be less 60 characters',
                ],
                'notification_balance' => [
                    'required' => 'Notification balance is missed',
                    'numeric' => 'Please input numeric value',
                    'greater_than' => 'Notification balance must be more or equal 0'
                ]
            ];

            $validation->setRules($rules, $errors);

            if (!$validation->run($this->request->getVar())) {
                $this->response(['status' => 0, 'errors' => $validation->getErrors()]);
            } else {
                $model = new UserModel();

                $newData = [
                    'login' => $this->request->getVar('login'),
                    'contact' => $this->request->getVar('contact'),
                    'contact_type' => $this->request->getVar('contact_type'),
                    'notification' => $this->request->getVar('notification_state') ? 1 : 0,
                    'notification_balance' => (int)$this->request->getVar('notification_balance')
                ];

                try {
                    $model->update($this->session->get('id'), $newData);
                } catch (ReflectionException $e) {
                    die($e->getMessage());
                }

                $this->response(['status' => 1]);
            }
        }
    }

    public function editPassword()
    {
        if ($this->request->isAJAX()) {

            $validation = Services::validation();

            $rules = [
                'password' => 'required|min_length[8]|max_length[255]|validatePassword[password]',
                'password_confirm' => 'required|matches[new_password]',
                'new_password' => 'required|min_length[8]|max_length[255]'
            ];

            $errors = [
                'password' => [
                    'required' => 'Password is missed',
                    'min_length' => 'Password must be more 8 characters',
                    'max_length' => 'Password must be less 255 characters',
                    'validatePassword' => 'Current password is not correct'
                ],
                'password_confirm' => [
                    'required' => 'Password confirm is missed',
                    'matches' => 'Password not equals'
                ],
                'new_password' => [
                    'required' => 'Password confirm is missed',
                    'matches' => 'Password not equals'
                ],
            ];

            $validation->setRules($rules, $errors);

            if (!$validation->run($this->request->getVar())) {
                $this->response(['status' => 0, 'errors' => $validation->getErrors()]);
            } else {
                $model = new UserModel();

                $newData = [
                    'password' => $this->request->getVar('new_password'),
                ];

                try {
                    $model->update($this->session->get('id'), $newData);
                } catch (ReflectionException $e) {
                    die($e->getMessage());
                }

                $this->response(['status' => 1]);
            }
        }
    }
}
