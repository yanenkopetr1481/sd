<?php

namespace App\Controllers;

use App\Models\UserModel;
use Config\Services;
use ReflectionException;

class Login extends BaseController
{
    public function index()
    {
        return view('login/login');
    }

    public function login()
    {
        helper(['form']);

        if ($this->request->isAJAX()) {

            $validation = Services::validation();

            $rules = [
                'login' => 'required|min_length[3]|max_length[50]',
//                'h-captcha-response' => 'required|min_length[6]|validateCaptcha[h-captcha-response]',
                'password' => 'required|min_length[8]|max_length[255]|validateUser[login,password]'
            ];

            $errors = [
                'password' => [
                    'validateUser' => 'Логин или пароль не совпадают',
                    'required' => 'Пароль отсутствует',
                    'min_length' => 'Пароль должен содержать более 8 символов',
                    'max_length' => 'Пароль должен быть меньше 255 символов',
                ],
                'h-captcha-response' => [
                    'validateCaptcha' => 'Неверная капча',
                    'required' => 'Captcha пропущена'
                ],
                'login' => [
                    'required' => 'Логин отсутствует',
                    'min_length' => 'Логин должен содержать более 3 символов',
                    'max_length' => 'Логин должен быть меньше 50 символов',
                ]
            ];

            $validation->setRules($rules, $errors);

            if (!$validation->run($this->request->getVar())) {
                $this->response(['status' => 0, 'errors' => $validation->getErrors()]);
            } else {

                $model = new UserModel();

                $user = $model->where('login', $this->request->getVar('login'))
                    ->first();

                $this->setUserSession($user);
                $this->response(['status' => 1]);
            }
        }
    }

    private function setUserSession($user)
    {
        $data = [
            'id' => $user['id'],
            'login' => $user['login'],
            'balance' => $user['balance'],
            'isLoggedIn' => true,
            'isAdmin' => $user['isAdmin'],
            'contact' => $user['contact'],
            'contact_type' => $user['contact_type'],
        ];

        $this->session->set($data);
        return true;
    }

    public function register(): void
    {
        helper(['form']);

        if ($this->request->isAJAX()) {

            $validation = Services::validation();

            $rules = [
                'login' => 'required|min_length[3]|max_length[20]',
                'password' => 'required|min_length[8]|max_length[255]',
                'password_confirm' => 'required|matches[password]',
                'secret_key' => 'required|min_length[3]|max_length[255]',
                'contact' => 'required|min_length[3]|max_length[255]',
                'h-captcha-response' => 'required|min_length[6]|validateCaptcha[h-captcha-response]'
            ];

            $errors = [
                'password' => [
                    'required' => 'Пароль отсутствует',
                    'min_length' => 'Пароль должен содержать более 8 символов',
                    'max_length' => 'Пароль должен быть меньше 255 символов',
                ],
                'password_confirm' => [
                    'required' => 'Подтверждение пароля отсутствует',
                    'match' => 'Пароль не равен'
                ],
                'h-captcha-response' => [
                    'validateCaptcha' => 'Неверная капча',
                    'required' => 'Captcha пропущена'
                ],
                'login' => [
                    'required' => 'Логин отсутствует',
                    'min_length' => 'Логин должен содержать более 3 символов',
                    'max_length' => 'Логин должен быть меньше 50 символов',
                ],
                'secret_key' => [
                    'required' => 'Секретное слово отсутствует',
                    'min_length' => 'Секретное слово должно быть больше 3 символов',
                    'max_length' => 'Секретное слово должно быть меньше 255 символов',
                ],
                'contact' => [
                    'required' => 'Контакт отсутствует',
                    'min_length' => 'Контакт должен содержать более 3 символов',
                    'max_length' => 'Контакт должен быть меньше 255 символов',
                ]
            ];

            $validation->setRules($rules, $errors);

            if (!$validation->run($this->request->getVar())) {
                $this->response(['status' => 0, 'errors' => $validation->getErrors()]);
            } else {
                $model = new UserModel();

                $newData = [
                    'login' => $this->request->getVar('login'),
                    'password' => $this->request->getVar('password'),
                    'secret_key' => $this->request->getVar('secret_key'),
                    'contact' => $this->request->getVar('contact'),
                    'contact_type' => $this->request->getVar('contact_type'),
                ];

                try {
                    $model->save($newData);
                } catch (ReflectionException $e) {
                    die($e->getMessage());
                }

                $this->response(['status' => 1]);
            }
        }
    }

    public function logout()
    {
        $this->session->destroy();
    }

}
