<?php namespace App\Controllers;


use App\Models\CreditModel;
use App\Models\OrderModel;
use App\Models\UserModel;
use Plisio\ClientAPI;
use ReflectionException;

class Psilio extends BaseController
{
    public function getBalance()
    {

        $plisio = new ClientAPI($this->secretKey);

        $plisio->getBalances('BTC');
    }

    public function success()
    {
        $this->logger->critical('Success Response: ' . $this->request->getBody());
    }

    public function failed()
    {
        $this->logger->error('Failed Response: ' . $this->request->getBody());
    }

    public function status()
    {
        $plisio = new ClientAPI($this->secretKey);

        $data = $_POST;

        if ($plisio->verifyCallbackData($data, $this->secretKey)) {

            $order_model = new OrderModel();

            $order_data = $order_model->getWhere(['id' => $data['order_number']])->getRowArray();

            if ($order_data['user_id'] === $data['order_name']) {
                $user_model = new UserModel();
                $user_data = $user_model->getWhere(['id' => $order_data['user_id']])->getRowArray();

                $credit_model = new CreditModel();
                $credit_data = $credit_model->getWhere(['id' => $order_data['credit_id']])->getRowArray();

                $balance = (int) $user_data['balance'];
                $new_balance = $balance + (int) $credit_data['cost'] + (int) $credit_data['bonus'];

                $order_status = strtolower($order_model['status']);

                if ($order_status != 'completed' && $order_status != 'mismatch') {
                    if ($data['status'] == 'completed' || $data['status'] == 'mismatch') {
                        try {
                            $user_model->update($order_data['user_id'], ['balance' => $new_balance]);
                            $order_model->update($data['order_number'], ['amount_paid' => $data['amount'], 'status' => 'completed']);
                        } catch (ReflectionException $e) {
                            die($e->getMessage());
                        }
                    }
                    else {
                        try {
                            $result_status = floatval($data['source_amount']) < floatval($credit_data['usd']) ? 'not_full' : 'expired';
                            $order_model->update($data['order_number'], ['amount_paid' => $data['amount'], 'status' => $result_status]);
                        } catch (ReflectionException $e) {
                            die($e->getMessage());
                        }
                    }
                }
            }
        }

        $this->logger->error('Status Response POST: ' . $_POST);
    }
}


