<?php

namespace App\Controllers;

use App\Models\ApiModel;
use App\Models\CreditModel;
use App\Models\DashboardModel;
use App\Models\OrderModel;
use App\Models\TargetModel;
use App\Models\UserModel;
use App\Models\VerificationHistory;
use Config\Services;
use ReflectionException;
use Throwable;

class Admin extends BaseController
{
    public function index()
    {
        return $this->loadView('dashboard');
    }

    public function getUsers()
    {
        if ($this->session->get('isAdmin')) {
            $model = new UserModel();
            $user_data = $model->get()->getResultArray();

            if (empty($user_data)) {
                return view('errors/html/error');
            }

            return $this->loadView('admin/users', ['data' => $user_data, 'selected' => 'users_page'], '<div class="col-10 content_block">', '</div>');
        }

        return view('errors/html/error');
    }

    public function getSettings()
    {
        if ($this->session->get('isAdmin')) {

            return $this->loadView('admin/settings', ['selected' => 'settings_page'], '<div class="col-10 content_block">', '</div>');
        }

        return view('errors/html/error');
    }


    public function getSettingsData()
    {
        if ($this->session->get('isAdmin')) {
            $model = new ApiModel();
            $settings_data = $model->get()->getResultArray();

            $settingsTotalData = [];

            if (empty($settings_data)) {
                return view('errors/html/error');
            } else {
                $api = new Api();

                foreach ($settings_data as $setting) {
                    if ($setting['token']) {

                        try {
                            $data = $api->getAccountData($setting['token']);
                            $data['token'] = $setting['token'];
                            $data['id'] = $setting['id'];
                            array_push($settingsTotalData, $data);
                        }
                        catch (Throwable $exception) {}
                    }
                }
            }

            return $this->response([
                'status' => 1,
                'content' => view('admin/settingsData', ['data' => $settingsTotalData])
            ]);
        }

        return $this->response([
            'status' => 0,
        ]);
    }


    public function editUserByAdmin()
    {
        if ($this->session->get('isAdmin')) {
            $model = new UserModel();

            $newData = [
                'login' => $this->request->getVar('login'),
                'contact' => $this->request->getVar('contact'),
                'contact_type' => $this->request->getVar('contact_type'),
                'balance' => $this->request->getVar('balance'),
                'secret_key' => $this->request->getVar('secret_key')
            ];

            try {
                $model->update($this->request->getVar('id'), $newData);
            } catch (ReflectionException $e) {
                die($e->getMessage());
            }

            $this->response(['status' => 1]);
        }
    }

    public function addApiUserByAdmin()
    {
        if ($this->session->get('isAdmin')) {
            $model = new ApiModel();

            $newData = [
                'token' => $this->request->getVar('token'),
                'email' => $this->request->getVar('email'),
            ];

            try {
                $model->insert($newData);
            } catch (ReflectionException $e) {
                die($e->getMessage());
            }

            $this->response(['status' => 1]);
        }
    }

    public function editApiUserByAdmin()
    {
        if ($this->session->get('isAdmin')) {
            $model = new ApiModel();

            $newData = [
                'token' => $this->request->getVar('token'),
                'email' => $this->request->getVar('email'),
            ];

            try {
                $model->update($this->request->getVar('id'), $newData);
            } catch (ReflectionException $e) {
                die($e->getMessage());
            }

            $this->response(['status' => 1]);
        }
    }

    public function editPostByAdmin()
    {
        if ($this->session->get('isAdmin')) {
            $model = new DashboardModel();

            $newData = [
                'title' => $this->request->getVar('title'),
                'content' => $this->request->getVar('content'),
            ];

            try {
                $model->update($this->request->getVar('id'), $newData);
            } catch (ReflectionException $e) {
                die($e->getMessage());
            }

            $this->response(['status' => 1]);
        }
    }

    public function removePost()
    {
        if ($this->session->get('isAdmin')) {
            $model = new DashboardModel();
            $model->delete($this->request->getVar('id'));

            $this->response(['status' => 1]);
        }
    }

    public function removeCredit()
    {
        if ($this->session->get('isAdmin')) {
            $model = new CreditModel();
            $model->delete($this->request->getVar('id'));

            $this->response(['status' => 1]);
        }
    }

    public function createPostByAdmin()
    {
        if ($this->session->get('isAdmin')) {
            $model = new DashboardModel();

            $newData = [
                'title' => $this->request->getVar('title'),
                'content' => $this->request->getVar('content'),
            ];

            try {
                $model->insert($newData);
            } catch (ReflectionException $e) {
                die($e->getMessage());
            }

            $this->response(['status' => 1]);
        }
    }

    public function createCreditByAdmin()
    {
        if ($this->session->get('isAdmin')) {
            $model = new CreditModel();

            $newData = [
                'usd' => $this->request->getVar('usd'),
                'cost' => $this->request->getVar('cost'),
                'bonus' => $this->request->getVar('bonus')
            ];

            try {
                $model->insert($newData);
            } catch (ReflectionException $e) {
                die($e->getMessage());
            }

            $this->response(['status' => 1]);
        }
    }

    public function editCreditByAdmin()
    {
        if ($this->session->get('isAdmin')) {
            $model = new CreditModel();

            $newData = [
                'usd' => $this->request->getVar('usd'),
                'cost' => $this->request->getVar('cost'),
                'bonus' => $this->request->getVar('bonus')
            ];

            try {
                $model->update($this->request->getVar('id'), $newData);
            } catch (ReflectionException $e) {
                die($e->getMessage());
            }

            $this->response(['status' => 1]);
        }
    }

    public function getUserHistory($id)
    {
        if ($this->session->get('isAdmin')) {
            $order_model = new OrderModel();
            $order_model->select('orders_history.id,orders_history.invoice_url, orders_history.payment_type, orders_history.timestamp, orders_history.amount_paid, credits.cost, credits.usd,orders_history.status,orders_history.amount_request');
            $order_model->join('credits', 'credits.id = orders_history.credit_id', 'LEFT')->orderBy('orders_history.id', 'DESC');
            $data = $order_model->getWhere([
                'orders_history.user_id' => $id,
            ], 1000, 0, '')->getResultArray();


            return $this->loadView('admin/order_history', ['data' => $data], '<div class="col-10 content_block">', '</div>');
        }

        return view('errors/html/error');
    }

    public function getUserOrderHistory($id)
    {
        $model = new TargetModel();

        $verification_model = new VerificationHistory();
        $data = $verification_model->getWhere(['user_id' => $id])->getResultArray();

        $data = array_reverse($data, true);

        foreach ($data as &$verifications) {

            $target_icon = $model->getWhere(['name' => $verifications['data']['target_name']])->getFirstRow('array');
            $verifications['data']['iconUri'] = $target_icon['iconUri'] ? 'https://www.phoneblur.com' . $target_icon['iconUri'] : 'https://img.icons8.com/color/96/000000/service.png';
        }

        return $this->loadView('history', ['data' => $data], '<div class="col-10 content_block">', '</div>');
    }
}
