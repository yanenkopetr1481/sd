<?php namespace App\Controllers;

use App\Models\DashboardModel;
use App\Models\FavouriteTargets;
use App\Models\TargetModel;
use App\Models\CreditModel;
use Plisio\ClientAPI;
use ReflectionException;

class Dashboard extends BaseController
{
    public function index()
    {
        $model = new DashboardModel();
        $cards = $model->orderBy('id', 'DESC')->get()->getResultArray();

        return $this->loadView('dashboard', ['data' => $cards, 'selected' => 'announcement_page']);
    }

    public function getVerificationsBlock()
    {
        $favorite_tags = [];

        $model = new TargetModel();

        $favourite_model = new FavouriteTargets();

        $auth = new Api();

        $favourites = $favourite_model->select('target_id')->getWhere(['user_id' => session()->get('id')])->getResultArray();

        foreach ($favourites as $favourite_tag) {
            array_push($favorite_tags, $favourite_tag['target_id']);
        }

        $data = $auth->checkService('');

        $new_rental_data = [];
        foreach ($data['availableServices'] as $rental) {
            $target_icon = $model->getWhere(['name' => $rental])->getFirstRow('array');
            $new_data['iconUri'] = ($target_icon != null && $target_icon['iconUri']) ? 'https://www.phoneblur.com' . $target_icon['iconUri'] : 'https://img.icons8.com/color/96/000000/service.png';
            $new_data['status'] = lang('General.' . STATUSES[4], [], $this->language->getLocale());
            $new_data['cost'] = 1;
            $new_data['name'] = str_replace('_', ' ', $rental);
            $new_data['id'] = $rental;
            $new_data['favourite'] = in_array($rental, $favorite_tags) ? 0 : 1;
            array_push($new_rental_data, $new_data);
        }

        $names = [];

        foreach ($new_rental_data as $key => $row) {
            $names[$key] = $row['name'];
        }

        $zip = '';
        foreach ($data['availableZips'] as $zipCode) {
            $zip .= '<li><a role="option" class="dropdown-item"><span class="text">' . $zipCode . '</span></a></li>';
        }

        array_multisort($names, SORT_ASC, $new_rental_data);

        $favourites_sort = [];

        if ($favourites !== null && !empty($favourites)) {
            foreach ($new_rental_data as $key => $row) {
                $favourites_sort[$key] = $row['favourite'];
            }

            array_multisort($favourites_sort, SORT_NUMERIC, $new_rental_data);
        }

        $this->response(['zip' => $zip, 'content' => view('verifications_body', ['data' => $new_rental_data, 'favourite_tags' => $favorite_tags])]);
    }

    public function addToFavourite()
    {
        if ($this->request->isAJAX()) {

            $model = new FavouriteTargets();

            $targetID = $this->request->getVar('targetID');

            $data = $model->getWhere(['user_id' => (int) session()->get('id'), 'target_id' => $targetID])->getRowArray();

            if ($data == null) {
                try {
                    $model->insert(['user_id' => (int)session()->get('id'), 'target_id' => $targetID]);
                    $this->response(['success' => 1]);
                } catch (ReflectionException $e) {
                    die($e->getMessage());
                }
            } else {
                $response = $model->delete(['id' => $data['id']])->resultID;
                $this->response(['success' => 2, 'deleted' => $response]);
            }
        }
    }

    public function getCredits()
    {
        $model = new CreditModel();
        $credits = $model->findAll();

        $plisio = new ClientAPI($this->secretKey);
        $cryptocurrencies = $plisio->getCurrencies();

        return $this->loadView('credits', ['data' => $credits, 'cryptocurrencies' => $cryptocurrencies, 'selected' => 'buy_page'], '<div class="col-10 content_block">', '</div>');
    }

    public function changeLanguage()
    {
        $lang = $this->request->getVar('lang');
        $this->session->remove('lang');
        $this->session->set('lang', $lang);
    }
}
