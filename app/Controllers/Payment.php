<?php namespace App\Controllers;

use App\Config\ApiConfig;
use App\Models\CreditModel;
use App\Models\OrderModel;
use App\Models\TargetModel;
use App\Models\UserModel;
use App\Models\VerificationHistory;
use Plisio\ClientAPI;
use ReflectionException;
use Throwable;

class Payment extends BaseController
{
    public function getPurchase()
    {
        if ($this->request->isAJAX()) {
            $id = $this->request->getVar('id');
            $currency = $this->request->getVar('currency');
            $currency_amount = $this->request->getVar('crypto_amount');

            $model = new CreditModel();
            $data = $model->getWhere(['id' => (int)$id])->getFirstRow('array');

            $this->response(['content' => view('purchase', ['data' => $data, 'currency' => $currency, 'currency_amount' => $currency_amount])]);
        }
    }

    public function getHistory()
    {
        $model = new TargetModel();

        $verification_model = new VerificationHistory();
        $data = $verification_model->getWhere(['user_id' => $this->session->get('id')])->getResultArray();

        $data = array_reverse($data, true);

        foreach ($data as &$verifications) {

            $target_icon = $model->getWhere(['name' => $verifications['data']['target_name']])->getFirstRow('array');
            $verifications['data']['iconUri'] = $target_icon['iconUri'] ? 'https://www.phoneblur.com' . $target_icon['iconUri'] : 'https://img.icons8.com/color/96/000000/service.png';
        }

        return $this->loadView('history', ['data' => $data], '<div class="col-10 content_block">', '</div>');
    }

    public function checkPhones()
    {
        $verificationModel = new VerificationHistory();
        $result = $verificationModel->getWhere(['user_id' => $this->session->get('id'), 'status' => 1])->getResultArray();

        $api = new Api();

        foreach ($result as $r) {
            $transaction_data = $api->getAccountData($r['token'])['transactions'];

            foreach ($transaction_data as $transaction) {
                $status = strpos($transaction['description'], $r['verification_id']);

                if ($status && $transaction['amount'] > 0) {
                    try {
                        $userModel = new UserModel();
                        $user = $userModel->getWhere(['id' => $r['user_id']])->getRowArray();
                        $userModel->update(['id' => $r['user_id']], ['balance' => $user['balance'] + $transaction['amount']]);
                        $verificationModel->update(['id' => $r['id']], ['status' => 0]);
                    } catch (ReflectionException $e) {
                    }
                }
            }
        }
    }

    public function getVerifications()
    {
        $this->checkPhones();
        return $this->loadView('verification', ['selected' => 'verification_page'], '<div class="col-10 content_block">', '</div>');
    }

    public function getPayment()
    {
        if ($this->request->isAJAX()) {
            $id = $this->request->getVar('id');

            $date = date_create($this->request->getVar('timestamp'));
            $date = date_format($date, "Y-m-d H:i:s");

            $order_model = new OrderModel();

            try {
                $order = $order_model->save([
                    'credit_id' => (int)$id,
                    'user_id' => (int)session()->get('id'),
                    'payment_type' => $this->request->getVar('currency'),
                    'amount_request' => $this->request->getVar('currency_amount'),
                    'status' => 'New',
                    'timestamp' => $date,
                ]);

            } catch (ReflectionException $e) {
                die($e->getMessage());
            }

            if ($order) {
                $model = new CreditModel();

                $data = $model->getWhere(['id' => (int)$id])->getFirstRow('array');

                $request = [
                    'source_currency' => 'USD',
                    'source_amount' => $data['usd'],
                    'currency' => $this->request->getVar('currency'),
                    'order_name' => $this->session->get('id'),
                    'order_number' => strval($order_model->getInsertID())
                ];

                $plisio = new ClientAPI($this->secretKey);
                $invoice = $plisio->createTransaction($request);

                if ($invoice && isset($invoice['status']) && $invoice['status'] == 'success') {
                    try {
                        if ($order_model->update($order_model->getInsertID(), ['invoice_url' => $invoice['data']['invoice_url']])) {
                            $this->response(['status' => 1, 'url' => $invoice['data']['invoice_url'], 'plisio_data' => $invoice]);
                        } else {
                            $this->response(['status' => 0, 'data' => $invoice]);
                        }
                    } catch (ReflectionException $e) {
                        die($e->getMessage());
                    }
                } else {
                    $order_model->delete($order_model->getInsertID());
                    $this->response(['status' => 0, 'data' => $invoice]);
                }
            } else {
                $this->response(['status' => 0]);
            }
        }
    }

    public function getOrderHistory()
    {
        $date = $this->request->getVar('date');

        $date = date_create($date);
        $date = date_format($date, "Y-m-d H:i:s");

        $order_model = new OrderModel();
        $order_model->select('orders_history.id,orders_history.invoice_url, orders_history.payment_type, orders_history.timestamp, orders_history.amount_paid, credits.cost, credits.usd,orders_history.status,orders_history.amount_request');
        $order_model->join('credits', 'credits.id = orders_history.credit_id', 'LEFT')->orderBy('orders_history.id', 'DESC');
        $data = $order_model->getWhere([
            'orders_history.user_id' => session()->get('id'),
        ], 1000, 0, '')->getResultArray();

        return $this->loadView('order_history', ['data' => $data, 'current_date' => $date], '<div class="col-10 content_block">', '</div>');
    }

    public function getVerificationStatus($id = '')
    {
        $api = new Api();
        $config = new ApiConfig();

        if (!empty($id)) {

            $verificationModel = new VerificationHistory();
            $result = $verificationModel->getWhere(['verification_id' => $id, 'user_id' => $this->session->get('id')])->getRowArray();

            if ($result) {
                try {
                    $verification = $api->getPhoneData($result['token']);
                } catch (Throwable $exception) {
                    return $this->loadView('errors/html/error', ['selected' => '']);
                }

                return $this->loadView('sms_preview', ['data' => $verification, 'api_url' => $config->api_url, 'selected' => '']);
            }
        }

        return $this->loadView('errors/html/error');
    }

    public function createRentalOrderPreview()
    {
        if ($this->request->isAJAX()) {

            $api = new Api();
            $api->user_id = $this->session->get('id');

            $model = new UserModel();

            $user = $model->where('id', $api->user_id)
                ->first();

            $services = explode(',', $this->request->getVar('services'));
            $zip = $this->request->getVar('zip');

            $data['availableServices'] = $services;

            if (!empty($zip)) {
                $data['availableZips'] = $zip;
            }

            if ((float)$user['balance'] > count($services)) {

                try {
                    $checkService = $api->checkService(json_encode($data));
                } catch (Throwable $exception) {
                    $this->response(['status' => 0, 'msg' => lang('General.serverError', [], $this->language->getLocale())]);
                    die();
                }

                if ($checkService['available']) {

                    $checkService = $api->selectOrChangeService(json_encode([
                        'services' => $data['availableServices'],
                        'zip' => $data['availableZips'] ?? 'Any',
                    ]), $this->session->get('isPhoneUsing') ? $this->session->get('phoneToken') : '');

                    if (!isset($checkService['error']) && isset($checkService['phoneNumber'])) {

                        $verificationModel = new VerificationHistory();

                        try {
                            $model->update(['id' => $api->user_id], ['balance' => (float) $user['balance'] - 1]);
                            $id = $verificationModel->insert(
                                [
                                    'verification_id' => $checkService['phoneNumber'],
                                    'user_id' => $this->session->get('id'),
                                    'token' => $checkService['token'],
                                    'status' => 1,
                                    'services' => json_encode($data),
                                ]);
                        } catch (Throwable $e) {
                            $this->response(['status' => 0, 'msg' => lang('General.serverError', [], $this->language->getLocale())]);
                            die();
                        }

                        if ($id) {
                            $url = base_url() . '/verification/' . $checkService['phoneNumber'];
                            $this->response(['status' => 1, 'url' => $url]);
                            $this->checkPhones();
                        } else {
                            $this->response(['status' => 0, 'msg' => lang('General.fillBalance', [], $this->language->getLocale())]);
                        }
                    }
                } else {
                    $this->response(['status' => 0, 'msg' => lang('General.serverError', [], $this->language->getLocale())]);
                    die();
                }

            } else {
                $this->response(['status' => 0, 'msg' => lang('General.fillBalance', [], $this->language->getLocale())]);
            }
        }
    }
}

