<?php namespace App\Controllers;

use  App\Config\ApiConfig;
use App\Models\ApiModel;
use CodeIgniter\Controller;
use Config\Services;
use ReflectionException;

class Api extends BaseController
{
    private $config;
    public $user_id;

    public function getAccountData($key)
    {
        $this->config = new ApiConfig();

        $options = [
            'baseURI' => $this->config->api_url . 'api/account',
            'timeout' => 3
        ];

        Services::reset(true);
        $client = Services::curlrequest($options);
        $client->appendHeader('X-API-Key', $key);
        $response = $client->request('GET', '')->getBody();

        return json_decode($response, true);
    }

    public function checkService($data)
    {
        $this->config = new ApiConfig();

        $options = [
            'baseURI' => $this->config->api_url . 'api/checkService'
        ];

        $key = $this->getApiKey();

        if ($key) {
            Services::reset(true);
            $client = Services::curlrequest($options);
            $client->setBody($data);
            $client->appendHeader('X-API-Key', $key);
            $client->appendHeader('Accept', 'application/json');

            $response = $client->request('POST', '')->getBody();

            return json_decode($response, true);
        } else {
            return json_decode('', true);
        }
    }

    public function getPhoneData($key)
    {
        $this->config = new ApiConfig();

        $options = [
            'baseURI' => $this->config->api_url . 'api/line',
            'timeout' => 3
        ];

        $client = Services::curlrequest($options);
        $client->appendHeader('X-API-Key', $key);
        $response = $client->request('GET', '')->getBody();

        return json_decode($response, true);
    }

    public function selectOrChangeService($data, $key = '')
    {
        $this->config = new ApiConfig();

        $options = [
            'baseURI' => $this->config->api_url . 'api/line/changeService'
        ];

        $key = !empty($key) ? $key : $this->getApiKey(true);

        if ($key) {

            Services::reset(true);
            $client = Services::curlrequest($options);

            $client->setBody($data);
            $client->appendHeader('X-API-Key', $key);
            $client->appendHeader('Accept', 'application/json');
            $client->appendHeader('Content-Type', 'application/json');
            $response = json_decode($client->request('POST', '')->getBody(), true);
            $response['token'] = $key;
            return $response;
        } else {
            return json_decode('', true);
        }
    }

    public function fetchLineStatus($key)
    {
        $this->config = new ApiConfig();

        $options = [
            'baseURI' => $this->config->api_url . 'api/line',
        ];

        Services::reset(true);

        $client = Services::curlrequest($options);
        $client->appendHeader('Accept', 'application/json');
        $client->appendHeader('X-API-Key', $key);
        return json_decode($client->request('GET', '')->getBody(), true);
    }

    public function getApiKey($isNeedSet = false)
    {
        $apiModel = new ApiModel();

        if ($isNeedSet) {
            $targets = $apiModel->getWhere(['isAvaliable' => 1])->getResultArray();
        } else {
            $targets = $apiModel->get()->getResultArray();
        }

        $target = $targets[0] ?? [];
        $token = $target['token'] ?? false;

        $time = time();

        $deadlined = $apiModel->select('id')->getWhere(['deadline <' => $time, 'deadline != ' => null])->getResultArray();

        $ids = [];

        foreach ($deadlined as $d) {
            array_push($ids, $d['id']);
        }

        if (!empty($ids)) {

            try {
                $apiModel->update($ids, [
                    'isAvaliable' => 1,
                    'deadline' => null,
                    'user_id' => 0
                ]);
            } catch (ReflectionException $e) {
            }
        }

        if ($isNeedSet) {

            $deadline = $time + 60 * 15;
            if ($deadline < (int)$target['deadline'] || $target['deadline'] == null) {
                try {
                    $apiModel->update($target['id'], [
                        'isAvaliable' => 0,
                        'deadline' => $deadline,
                        'user_id' => $this->user_id
                    ]);
                } catch (ReflectionException $e) {
                }
            }
        }

        return $token;
    }
}
