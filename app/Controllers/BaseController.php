<?php

namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 * @package CodeIgniter
 */

use App\Models\UserModel;
use App\Models\VerificationHistory;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Language\Language;
use CodeIgniter\Session\Session;
use Config\Services;
use Psr\Log\LoggerInterface;
use ReflectionException;
use Throwable;

const STATUSES = [
    1 => 'NotAvailable',
    4 => 'Available',
    8 => 'SurgePricingBlocked',
    128 => 'QuotaExceeded'
];

const PRICING_MODE = [
    1 => 'Default',
    2 => 'Surge',
    4 => 'Free',
    8 => 'Premium',
    16 => 'Adjusted',
    32 => 'Filtered'
];

class BaseController extends Controller
{
//    protected $secretKey = 'ONvlrP9_s1IEuToUWwcmwQSsWk8p6h_cRKmqaevxRMH6a06SFkb3xISTE2iAisgv';
    protected $secretKey = 'lUk0RgwnqIAU7L38sPSlmnmwIdRlLpvRaZYHLM2s9EZUvhlE2hLxMQzoiRracvwn';

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = [];
    /**
     * @var Session
     */

    protected $session;
    /**
     * @var Language
     */
    protected $language;

    protected $activeNumber = false;
    protected $activeNumberToken = '';

    /**
     * Constructor.
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param LoggerInterface $logger
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->session = Services::session();
        $this->language = Services::language();
        $lang = $this->session->get('lang') ?? $this->request->getVar('lang') ?? 'en';
        $this->language->setLocale($lang);
    }

    public function loadView($name, $data = [], $content_header = '', $content_footer = '')
    {
        $user_model = new UserModel();
        $user_data = $user_model->where('id', $this->session->get('id'))->get()->getRowArray();

        return view('templates/header', ['user_data' => $user_data, 'activePhone' => $this->checkActivePhones(), 'session' => $this->session, 'selected' => $data['selected'], 'balance' => $user_data['balance'], 'login' => $this->session->get('login')]) . $content_header . view($name, $data) . $content_footer . view('templates/footer');
    }

    public function checkActivePhones()
    {
        $verificationModel = new VerificationHistory();
        $phoneHistory = $verificationModel->getWhere(['user_id' => $this->session->get('id'), 'status' => 1])->getResultArray();

        $api = new Api();

        $isPhoneUsing = false;

        foreach ($phoneHistory as $phone) {
            try {
                $data = $api->fetchLineStatus($phone['token']);

                if (!isset($data['error']) && isset($data['phoneNumber'])) {
                    $isPhoneUsing = $data['phoneNumber'];
                    $this->session->set('phoneToken', $phone['token']);
                }

            } catch (Throwable $exception) {}
        }

        $this->session->set('isPhoneUsing', $isPhoneUsing);
        return $isPhoneUsing;
    }

    public function response($data = null)
    {
        $data['csrf'] = csrf_hash();
        echo json_encode($data);
    }
}
